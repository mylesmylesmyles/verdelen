CREATE TABLE public.entry_types
(
   id integer NOT NULL, 
   name character varying(40) NOT NULL, 
   CONSTRAINT entry_types_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entry_types
  OWNER TO verdelen;
COMMENT ON COLUMN public.entry_types.id IS 'The primary key';
COMMENT ON COLUMN public.entry_types.name IS 'The type name';
COMMENT ON TABLE public.entry_types
  IS 'Types of account entries';
