/**
*
* @file  /models/DashboardService.cfc
* @author Myles Goodhue
* @description A service for the dashboard
*
*/

component autowire=true accessors=true {

	property 	name="util"
				persistent=false
				inject="model:util";

	/**
	 * @hint Constructor
	 */
	DashboardService function init(){
		return this;
	}
	
	/**
	 * @hint List the budgets and accounts
	 */
	query function listBudgetsAndAccounts(required User user) {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			select b.id budget_id, 
				b.name budget_name, 
				a.id account_id, 
				a.name account_name
			from budgets b, accounts a
			where b.owner_users_id = ?
			and b.id = a.budgets_id
			order by budget_name, account_name
		");
		stmt.setString(1, arguments.user.id);
		stmt.executeQuery();
		return util.resultsetToQuery(stmt.getResultSet());
	}
	
	
	/**
	 * @hint List last 10 entries of a particular entry type
	 */
	query function listLast10Entries(required User user, required EntryType entryType) {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			SELECT 
				budgets.id as budgets_id,
				budgets.name as budgets_name,
				accounts.id as accounts_id,
				accounts.name as accounts_name,
				entries.id as entries_id,
				entries.amount,
				entries.occurred_at as occurredAt,
				entries.entry_types_id as typeId,
				entry_types.name as typeName,
				entries.notes,
				entries.created_at as createdAt,
				entries.created_users_id as createdById,
				c_users.first_name || ' ' || c_users.last_name as createdByName,
				entries.modified_users_id as modifiedById,
				m_users.first_name || ' ' || m_users.last_name as modifiedByName,
				entries.modified_at as modifiedAt
			FROM 
				budgets, 
				accounts, 
				entries,
				entry_types,
				users c_users,
				users m_users
			WHERE 
				accounts.budgets_id = budgets.id AND
				entries.accounts_id = accounts.id AND
				entry_types.id = entries.entry_types_id AND
				entries.created_users_id = c_users.id AND
				entries.modified_users_id = m_users.id AND
				entries.entry_types_id = ? AND 
				budgets.owner_users_id = ?
			ORDER BY
			  entries.occurred_at DESC
			LIMIT 10
		");
		stmt.setInt(1, javaCast('int', arguments.entryType.id));
		stmt.setString(2, arguments.user.id);
		stmt.executeQuery();
		return util.resultsetToQuery(stmt.getResultSet());
	}
	
	
}