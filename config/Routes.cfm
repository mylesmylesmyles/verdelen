﻿	<cfscript>
	// Allow unique URL or combination of URLs, we recommend both enabled
	setUniqueURLS(false);
	// Auto reload configuration, true in dev makes sense to reload the routes on every request
	//setAutoReload(false);
	// Sets automatic route extension detection and places the extension in the rc.format variable
	setExtensionDetection(true);
	// The valid extensions this interceptor will detect
	setValidExtensions('json,jsont,html,htm');
	// If enabled, the interceptor will throw a 406 exception that an invalid format was detected or just ignore it
	setThrowOnInvalidExtension(true);

	// Base URL
	proto = 'http';
	if (cgi.SERVER_PORT_SECURE neq 0) {
		proto = 'https';
	}
	if( len(getSetting('AppMapping') ) lte 1){
		setBaseURL("#proto#://#cgi.HTTP_HOST#/index.cfm");
	}
	else{
		setBaseURL("#proto#://#cgi.HTTP_HOST#/#getSetting('AppMapping')#/index.cfm");
	}

	// Repeat Entries -------------------------------------------------
	addNamespace(pattern="/accounts/:accountid/repeatentries", namespace="repeatentries");

	addRoute(namespace="repeatentries", pattern="/:id/edit", handler="repeatentries", action={
			GET = "edit"
		});

	addRoute(namespace="repeatentries", pattern="/:id/delete", handler="repeatentries", action={
			GET = "verifyDelete", POST = "delete"
		});

	addRoute(namespace="repeatentries", pattern="/new", handler="repeatentries", action={
			GET = "createForm", POST = "create"
		});

	addRoute(namespace="repeatentries", pattern="/:id", handler="repeatentries", action={
			GET = "view", POST = "update"
		});

	addRoute(namespace="repeatentries", pattern="/", handler="repeatentries", action="list");

	// Entries -------------------------------------------------
	addNamespace(pattern="/accounts/:accountid/entries", namespace="entries");

	addRoute(namespace="entries", pattern="/:id/delete", handler="entries", action={
			GET = "verifyDelete", POST = "delete"
		});

	addRoute(namespace="entries", pattern="/:id/edit", handler="entries", action={
			GET = "edit"
		});

	addRoute(namespace="entries", pattern="/new", handler="entries", action={
			GET = "createForm", POST = "create"
		});

	addRoute(namespace="entries", pattern="/:id", handler="entries", action={
			GET = "view", POST = "update"
		});

	addRoute(namespace="entries", pattern="/", handler="entries", action="list");


	// Accounts -------------------------------------------------
	addNamespace(pattern="/accounts", namespace="accounts");

	addRoute(namespace="accounts", pattern="/:id/delete", handler="accounts", action={
			GET = "verifyDelete", POST = "delete"
		});

	addRoute(namespace="accounts", pattern="/:id/sharing", handler="accounts", action={
			GET = "listSharing", POST = "updateSharing"
		});

	addRoute(namespace="accounts", pattern="/new", handler="accounts", action={
			GET = "createForm", POST = "create"
		});

	addRoute(namespace="accounts", pattern="/:id", handler="accounts", action={
			GET = "view", POST = "update"
		});

	addRoute(namespace="accounts", pattern="/", handler="accounts", action="list");

	// Account Transfers -------------------------------------------------
	addNamespace(pattern="/transfers", namespace="transfers");

	addRoute(namespace="transfers", pattern="/:id/delete", handler="transfers", action={
			GET = "verifyDelete", POST = "delete"
		});

	addRoute(namespace="transfers", pattern="/new", handler="transfers", action={
			GET = "createForm", POST = "create"
		});

	addRoute(namespace="transfers", pattern="/:id", handler="transfers", action={
			GET = "view", POST = "update"
		});

	addRoute(namespace="transfers", pattern="/", handler="transfers", action="list");

	// Users ------------------------------------------------------
	addNamespace(pattern="/users", namespace="users");

	addRoute(namespace="users", pattern="/myprofile/password", handler="user", action={
			GET = "profile", POST = "updatePassword"
		});

	addRoute(namespace="users", pattern="/:id/edit", handler="user", action={
			GET = "edit"
		});

	addRoute(namespace="users", pattern="/:id/delete", handler="user", action={
			GET = "verifyDelete", POST = "delete"
		});

	addRoute(namespace="users", pattern="/myprofile", handler="user", action={
			GET = "profile", POST = "update"
		});

	addRoute(namespace="users", pattern="/new", handler="user", action={
			GET = "createForm", POST = "create"
		});

	addRoute(namespace="users", pattern="/:id", handler="user", action={
			GET = "view", POST = "update"
		});

	addRoute(namespace="users", pattern="/", handler="user", action="list");

	// Schedule ---------------------------------------------------
	addNamespace(pattern="/schedule", namespace="schedule");

	addRoute(namespace="schedule", pattern="/", handler="schedule", action="index");

	addRoute(namespace="schedule", pattern="/run", handler="schedule", action="run");

	// Authentication ----------------------------------------------
	
	addRoute(pattern="/login", handler="authorization", action={
			GET = "login", POST = "validateCredentials"
		});
	
	addRoute(pattern="/logout", handler="authorization", action="logout");

	// Quick add entry form
	addRoute(pattern="/quickadd", handler="Dashboard", action={
		GET = "viewquickadd", POST = "savequickadd"
		});

	// General ------------------------------------------------------

	addRoute(pattern="/404", response="<h1>Oops!</h1>The page you are looking for was not found.", statusCode=404);
	addRoute(pattern="/403", response="<h1>Access denied</h1>You're not allowed to do that.", statusCode=403);
	addRoute(pattern="/", handler="Dashboard", action="index");


	/** Developers can modify the CGI.PATH_INFO value in advance of the SES
		interceptor to do all sorts of manipulations in advance of route
		detection. If provided, this function will be called by the SES
		interceptor instead of referencing the value CGI.PATH_INFO.

		This is a great place to perform custom manipulations to fix systemic
		URL issues your Web site may have or simplify routes for i18n sites.

		@Event The ColdBox RequestContext Object
	**/
	function PathInfoProvider(Event){
		/* Example:
		var URI = CGI.PATH_INFO;
		if (URI eq "api/foo/bar")
		{
			Event.setProxyRequest(true);
			return "some/other/value/for/your/routes";
		}
		*/
		return CGI.PATH_INFO;
	}
</cfscript>