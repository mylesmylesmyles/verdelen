CREATE TABLE public.accounts_transfers
(
   id character varying(36) NOT NULL, 
   amount double precision NOT NULL, 
   from_accounts_id character varying(36) NOT NULL, 
   to_accounts_id character varying(36) NOT NULL, 
   entry_frequency_id integer, 
   entry_frequencies_interval integer DEFAULT 0, 
   entry_frequencies_count integer DEFAULT 1, 
   entry_frequencies_start_at timestamp without time zone, 
   entry_frequencies_end_at timestamp without time zone, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT account_transfers_pk PRIMARY KEY (id), 
   CONSTRAINT account_transfers_from_accounts_fk FOREIGN KEY (from_accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_to_accounts_fk FOREIGN KEY (to_accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_frequencies_fk FOREIGN KEY (entry_frequency_id) REFERENCES entry_frequencies (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_created_users_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_modified_users_id FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.accounts_transfers
  OWNER TO verdelen;
COMMENT ON COLUMN public.accounts_transfers.id IS 'The primary key';
COMMENT ON COLUMN public.accounts_transfers.amount IS 'The amount being transfered';
COMMENT ON COLUMN public.accounts_transfers.from_accounts_id IS 'Transfering from this account';
COMMENT ON COLUMN public.accounts_transfers.to_accounts_id IS 'Transfering to this account';
COMMENT ON COLUMN public.accounts_transfers.entry_frequency_id IS 'The frequency of the transfer';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_interval IS 'The entry frequency interval';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_count IS 'The number of entires';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_start_at IS 'The start date/time when the transfers will begin';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_end_at IS 'The end date/time when the last transfer will occur';
COMMENT ON COLUMN public.accounts_transfers.notes IS 'Notes about the transfer';
COMMENT ON COLUMN public.accounts_transfers.created_at IS 'The date/time the record was created';
COMMENT ON COLUMN public.accounts_transfers.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.accounts_transfers.modified_at IS 'The date/time the record was last modified';
COMMENT ON COLUMN public.accounts_transfers.modified_users_id IS 'FK to USERS.ID';
COMMENT ON TABLE public.accounts_transfers
  IS 'Transfers from one account to another';


CREATE TABLE accounts_transfers_events
(
  id serial NOT NULL, -- Primary key
  accounts_transfers_id character varying(36) NOT NULL,
  repeat_year integer,
  repeat_month integer,
  repeat_week integer,
  repeat_day integer,
  repeat_weekday integer,
  CONSTRAINT accounts_transfers_events_pk PRIMARY KEY (id),
  CONSTRAINT accounts_transfers_events_fk FOREIGN KEY (accounts_transfers_id)
      REFERENCES accounts_transfers (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE accounts_transfers_events
  OWNER TO verdelen;
COMMENT ON COLUMN accounts_transfers_events.id IS 'Primary key';

