<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.budget" type="any";
    param name="prc.transfer" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center">Transfer from  <a href="#event.buildLink('accounts/#prc.transfer.fromAccount.id#')#"> #esapiEncode('html',prc.transfer.fromAccount.name)#</a> to <a href="#event.buildLink('accounts/#prc.transfer.toAccount.id#')#"> #esapiEncode('html',prc.transfer.toAccount.name)#</a></h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Amount</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #lsCurrencyFormat(prc.transfer.amount)#
      </div>
    </div>
    <cfif prc.transfer.hasfrequency()>
      <div class="row">
        <div class="col-xs-5 col-md-4 text-right">
          <strong>Frequency</strong>
        </div>
        <div class="col-xs-7 col-md-8">
          #prc.transfer.frequency.name#
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5 col-md-4 text-right">
          <strong>Count</strong>
        </div>
        <div class="col-xs-7 col-md-8">
          #prc.transfer.frequencyCount#
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5 col-md-4 text-right">
          <strong>Start</strong>
        </div>
        <div class="col-xs-7 col-md-8">
          #dateFormat(prc.transfer.frequencyStartAt, 'yyyy-mm-dd')#
        </div>
      </div>
      <div class="row">
        <div class="col-xs-5 col-md-4 text-right">
          <strong>End</strong>
        </div>
        <div class="col-xs-7 col-md-8">
          <cfif isNull(prc.transfer.frequencyEndAt)>
            Never
          <cfelse>
            #dateFormat(prc.transfer.frequencyEndAt, 'yyyy-mm-dd')#
          </cfif>
        </div>
      </div>
    </cfif>
    <cfif prc.transfer.hasEntry()>
      <div class="row">
        <div class="col-xs-5 col-md-4 text-right">
          <strong>Entries</strong>
        </div>
        <div class="col-xs-7 col-md-8">
          <ul>
            <cfloop array="#prc.transfer.entries#" item="entry">
              <li><a href="#event.buildLink('accounts/#entry.account.id#/entries/#entry.id#')#">#esapiEncode('html', entry.account.name)# account #entry.type.name.lcase()#</a></li>
            </cfloop>
          </ul>
        </div>
      </div>
    </cfif>

    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Notes</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.transfer.notes)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Created</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateFormat(prc.transfer.createdAt, 'yyyy-mm-dd')#
      </div>
    </div>

    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Last modified</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateFormat(prc.transfer.modifiedAt, 'yyyy-mm-dd')#
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <a href="#event.buildLink('transfers/#prc.transfer.id#/delete')#" class="btn btn-danger">Delete</a>
    </div>
</div>
</cfoutput>