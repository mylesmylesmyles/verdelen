/**
*
* @file  /handlers/Entries.cfc
* @author Myles Goodhue
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        view = "GET",
        list = "GET,POST",
        edit = "GET",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE"
    };

    void function preHandler(event) {
        prc.budget = entityNew('Budget').getCurrent(owner: prc.user);
        if (isNull(prc.budget)) {
            setNextEvent('404');
        }
        event.paramValue( name='accountid', value='' );
        prc.account = entityNew('Account').get(rc.accountid);
        if (isNull(prc.account.id) or prc.budget.id neq prc.account.budget.id) {
            setNextEvent('404');
        }
        if (prc.account.budget.owner.id neq prc.user.id) {
            setNextEvent('403');
        }
    }

    void function view(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('Entry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }
    }

    void function edit(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('Entry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }

        prc.xhSubmit = event.buildLink('accounts/#prc.account.id#/entries/#prc.entry.id#');
        prc.entryTypeList = entityNew('EntryType').list();
    }
    
    void function list(event,rc,prc) {
        event.paramValue( name='max', value='' );
        if (!isNumeric(rc.max) or rc.max eq 0) {
            rc.max = 10;
        }
        
        event.paramValue( name='offset', value='' );
        if (!isNumeric(rc.offset) or rc.offset <= 0) {
            rc.offset = 0;
        }
        
        prc.entryList = entityNew('Entry').list(criteria: {account = prc.account}, offset: rc.offset, max: rc.max);
        prc.entryTotal = prc.entryList.total;
                
    }
    
    void function update(event,rc,prc) {
        event.paramValue( name='id', value='' );
        var entry = entityNew('Entry').get(rc.id);
        if (isNull(entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq entry.account.id) {
            setNextEvent('404');
        }

        event.paramValue( name='amount', value='' );
        event.paramValue( name='occurredAt', value=now() );
        event.paramValue( name='type', value='' );
        event.paramValue( name='notes', value='' );
        
        var includeFields = ['amount,occurredAt,type,notes'];

        // Keep track of error messages
        var errorMessages = [];
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!isDate(rc.occurredAt)) {
            errorMessages.append("You must enter the date the entry occurred on.");
        }
        
        if (!isNumeric(rc.type)) {
            errorMessages.append("You must enter the entry type.");
        } else {
            var entryTypeObj = entityNew('EntryType').get(rc.type);
            if (isNull(entryTypeObj.id)) {
                errorMessages.append("The entry type selected is not valid.");
            }
        }
        
        if (notes.len() > 4000) {
            errorMessages.append("The notes must be less than 4000 characters.");
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('accounts/#rc.account.id#/entries/new');
            return;
        }

        transaction {
            prc.user = entityMerge(prc.user);
            entry = entityMerge(entry);
            entry.modifiedat = now();
            entry.modifiedby = prc.user;
            
            entry.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: true,
                composeRelationships: true
            );
            entry.save(flush: true);
        }
        setNextEvent('accounts/#prc.account.id#/entries/#entry.id#');
    }
    
    void function createForm(event,rc,prc) {
        prc.xhSubmit = event.buildLink('accounts/#prc.account.id#/entries/new');
        prc.entryTypeList = entityNew('EntryType').list();
    }
    
    void function create(event,rc,prc) {
        event.paramValue( name='amount', value='' );
        event.paramValue( name='occurredAt', value=now() );
        event.paramValue( name='type', value='' );
        event.paramValue( name='notes', value='' );
        
        var includeFields = ['amount,occurredAt,type,notes'];

        // Keep track of error messages
        var errorMessages = [];
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!isDate(rc.occurredAt)) {
            errorMessages.append("You must enter the date the entry occurred on.");
        }
        
        if (!isNumeric(rc.type)) {
            errorMessages.append("You must enter the entry type.");
        } else {
            var entryTypeObj = entityNew('EntryType').get(rc.type);
            if (isNull(entryTypeObj.id)) {
                errorMessages.append("The entry type selected is not valid.");
            }
        }
        
        if (notes.len() > 4000) {
            errorMessages.append("The notes must be less than 4000 characters.");
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('accounts/#rc.account.id#/entries/new');
            return;
        }

        transaction {
            var entry = entityNew('Entry').new({account: prc.account, createdby: prc.user, modifiedby: prc.user});
            entry.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: includeFields.toList(),
                composeRelationships: true
            );
            entry.save(flush: true);
        }
        setNextEvent('accounts/#prc.account.id#/entries/#entry.id#');
    }

    void function verifyDelete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('Entry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }
    }
    
    void function delete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('Entry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }
        transaction {
            entityNew('Entry').deleteById( rc.id );
        }

        setNextEvent('accounts/#prc.account.id#/entries');
    }
}