<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.budget" type="any";
    param name="prc.tranfersList" type="query";
    param name="rc.max" default="10";
    param name="rc.offset" default="0";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-10">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Transfers <span class="badge">#prc.tranfersList.total#</span></li>
          </ol>
        </div>
        <div class="col-sm-2 pull-right">
          <a href="#event.buildLink('transfers/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Transfer</a>
        </div>
      </div>
    </div>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>From Account</th>
          <th>To Account</th>
          <th>Amount</th>
          <th class="hidden-xs">Created</th>
        </tr>
      </thead>
      <tbody>
        <cfloop query="#prc.tranfersList#">
          <tr>
            <td class="text-center">
              <a href="#event.buildLink('transfers/#prc.tranfersList.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
        		</td>
            <td><a href="#event.buildLink('accounts/#prc.tranfersList.fromaccount_id#')#">#esapiEncode('html',prc.tranfersList.fromaccount_name)#</a></td>
            <td><a href="#event.buildLink('accounts/#prc.tranfersList.toaccount_id#')#">#esapiEncode('html',prc.tranfersList.toaccount_name)#</a></td>
            <td>#lsCurrencyFormat(prc.tranfersList.amount)#</td>
            <td class="hidden-xs">#dateFormat(prc.tranfersList.createdAt , 'yyyy-mm-dd')#</td>
        	</tr>
        </cfloop>
      </tbody>
    </table>
  </div>
</div>
<cfif prc.tranfersList.total gt 0>
  <cfmodule template="../paging.cfm" 
            offset="#rc.offset#" 
            max="#rc.max#" 
            recordCount="#prc.tranfersList.total#"
            url="#event.buildLink('transfers')#"/>
  </div>
</cfif>
</cfoutput>