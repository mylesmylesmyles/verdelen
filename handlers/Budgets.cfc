/**
*
* @file  /handlers/Budgets.cfc
* @author Myles Goodhue 
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        view = "GET",
        list = "GET,POST",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE"
    };

    void function preHandler(event) {
    }

    /**
     * @hint View a budget
     */
    void function view(event,rc,prc) {
        // Input validation
        event.paramValue( name='id', value='' );
        prc.budget = entityNew('Budget').get(rc.id);
        if (isNull(prc.budget.id)) {
            setNextEvent('404');
        }
        // prc.accounts = prc.budget.listAccounts();
        
        prc.xhCreateAccount = event.buildLink('accounts/new');
    }
    
    /**
     * @hint List all budgets belonging to a user
     */
    void function list(event,rc,prc) {
        prc.budgetList = entityNew('Budget').newCriteria()
            .eq("owner", prc.user)
            .list();
    }

    void function update(event,rc,prc) {}
    
    /**
     * @hint Dispaly the form for creating a new budget
     */
    void function createForm(event,rc,prc) {
        prc.xhSubmit = event.buildLink('budgets/new');
    }
    
    /**
     * @hint Create a new budget
     */
    void function create(event,rc,prc) {
        // Input validation
        event.paramValue( name='name', value='' );
        if (!rc.name.trim().len()) {
            // Keep track of error messages
            var errorMessages = [];
            errorMessages.append("You must enter a name.");
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('budgets/new');
            return;
        }

        // Save the new budget
        transaction {
            // Merge the user object into this session
            entityMerge(prc.user);
            
            // Populate and save the 
            var budget = entityNew('Budget').new({name=rc.name.trim(), owner=prc.user});
            budget.save(flush: true);
        }

        setNextEvent('budgets/#budget.id#');
    }
    
    void function verifyDelete(event,rc,prc) {}
    void function delete(event,rc,prc) {}
}