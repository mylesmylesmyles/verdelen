/**
*
* @file  /models/Util.cfc
* @author Myles Goodhue
* @description Model utility
*
*/

component autowire=true accessors=true {

	/**
	 * @hint Constructor
	 */
	Util function init(){
		return this;
	}

	/**
	 * @hint Convert a jdbc resultset to a query object
	 * @resultset.hint The resultset
	 * @name.hint The query name
	 */
	query function resultsetToQuery(required any resultset, string name="default") {
		return createObject('java', 'lucee.runtime.type.QueryImpl').init(arguments.resultset, arguments.name, javacast('null',0));
	}
	
	
}