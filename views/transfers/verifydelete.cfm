<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.transfer" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center">Delete transfer from <a href="#event.buildLink('transfers/#prc.transfer.fromAccount.id#')#">#esapiEncode('html',prc.transfer.fromAccount.name)#</a> to <a href="#event.buildLink('transfers/#prc.transfer.toAccount.id#')#">#esapiEncode('html',prc.transfer.toAccount.name)#</a></h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3">
    <p class="text-danger">Are you certain you want to delete this transfer?  This action can't be undone.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3 text-center">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <form action="#event.buildLink('transfers/#prc.transfer.id#/delete')#" method="post" class="form-horizontal">
      <a href="#event.buildLink('transfers/#prc.transfer.id#')#" class="btn btn-primary">Cancel</a>
      <input type="submit" value="Delete" class="btn btn-danger"/>
      </form>
    </div>
</div>
</cfoutput>