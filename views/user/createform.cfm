<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.xhSubmit" type="string";
    param name="prc.errorMessages" type="array" default=[];
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    <h2 class="text-center">Create a new user</h2>
    <br>
    <cfif prc.errorMessages.len()>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
        <ul>
          <cfscript>
            #prc.errorMessages.each(function (msg) {
              echo("<li>#msg#</li>")
            })#
          </cfscript>
        </ul>
      </div>
    </cfif>
    <form action="#prc.xhSubmit#" method="post" class="form-horizontal">
      <div class="form-group form-group-lg">
        <label for="firstName" class="col-sm-3 control-label">First Name</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-triangle-right"></span></span>
          <cfparam name="rc.firstName" default=""/>
          <input type="text" id="firstName" name="firstName" placeholder="Jane" value="#len(rc.firstName) ? rc.firstName : ''#" required class="form-control"/>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="lastName" class="col-sm-3 control-label">Last Name</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-triangle-right"></span></span>
          <cfparam name="rc.lastName" default=""/>
          <input type="text" id="lastName" name="lastName" placeholder="Doe" value="#len(rc.lastName) ? rc.lastName : ''#" required class="form-control"/>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="email" class="col-sm-3 control-label">Login Email</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
          <cfparam name="rc.email" default=""/>
          <input type="email" id="email" name="email" placeholder="jane.doe@example.com" value="#len(rc.email) ? rc.email : ''#" required class="form-control"/>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="password" class="col-sm-3 control-label">Password</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon">&bull;&bull;&bull;</span></span>
          <cfparam name="rc.password" default=""/>
          <input type="password" id="password" name="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;" value="#len(rc.password) ? rc.password : ''#" required class="form-control"/>
        </div>
      </div>
      <div class="row">
        <div class="text-center">
          <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-lg" value="Create user"/>
        </div>
      </div>
    </form>
  </div>
</div>
</cfoutput>