<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

  	param name="prc.budgetList" type="array";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-sm-10">
        <ol class="breadcrumb">
          <li><a href="/">Home</a></li>
          <li class="active">Budgets</li>
        </ol>
      </div>
      <div class="col-sm-2 pull-right">
        <a href="#event.buildLink('budgets/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Budget</a>
      </div>
    </div>

  </div>
  <table class="table table-hover">
    <thead>
      <tr>
        <th width="25%">Name</th>
        <th width="10%">Is Active</th>
        <th width="10%">Created</th>
        <th class="hidden-xs">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <cfloop array="#prc.budgetList#" index="budget">
      	<tr>
          <td>#esapiEncode('html',budget.name)#</td>
          <td>#(isBoolean(budget.isActive) and budget.isActive ? 'Yes' : 'No')#</td>
          <td>#dateFormat(budget.createdAt, 'yyyy-mm-dd')#</td>
      		<td class="text-center hidden-xs">
      			<a href="#event.buildLink('budgets/#budget.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
            <a href="#event.buildLink('budgets/#budget.id#/accounts')#" class="btn btn-info"><span class=" glyphicon glyphicon-list" aria-hidden="true"></span> Accounts (#entityNew('Account').newCriteria().eq("budget", budget).count()#)</a>
            <a href="#event.buildLink('budgets/#budget.id#/transfers')#" class="btn btn-info"><span class=" glyphicon glyphicon-list" aria-hidden="true"></span> Transfers</a>
            <cfif budget.isActive>
              <a href="#event.buildLink('budgets/#budget.id#/transfers/new')#" class="btn btn-success"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Transfer funds</a>
            </cfif>
      		</td>
      	</tr>
        <tr class="visible-xs">
          <td colspan="4" class="text-center">
            <a href="#event.buildLink('budgets/#budget.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
            <a href="#event.buildLink('budgets/#budget.id#/accounts')#" class="btn btn-info"><span class=" glyphicon glyphicon-list" aria-hidden="true"></span> Accounts (#entityNew('Account').newCriteria().eq("budget", budget).count()#)</a>
            <div class="clearfix visible-xs-block"></div>
            <a href="#event.buildLink('budgets/#budget.id#/transfers')#" class="btn btn-info"><span class=" glyphicon glyphicon-list" aria-hidden="true"></span> Transfers</a>
            <cfif budget.isActive>
              <a href="#event.buildLink('budgets/#budget.id#/transfers/new')#" class="btn btn-success"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Transfer funds</a>
            </cfif>
          </td>
        </tr>
      </cfloop>
    </tbody>
  </table>
  </div>
</div>
</cfoutput>