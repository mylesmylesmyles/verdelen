<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.entry" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center">Repeat <a href="#event.buildLink('accounts/#prc.entry.account.id#')#">#esapiEncode('html',prc.entry.account.name)#</a> entry </h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Amount</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #lsCurrencyFormat(prc.entry.amount)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Type</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.entry.type.name)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Frequency</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.entry.frequency.name)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Count</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.entry.frequencyCount)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Start</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateformat(prc.entry.frequencyStartAt, 'yyyy-mm-dd')#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>End</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        <cfif !isNull(prc.entry.frequencyEndAt)>
          #dateformat(prc.entry.frequencyEndAt, 'yyyy-mm-dd')#
        </cfif>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Notes</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.entry.notes)#
      </div>
    </div>
  </div>
</div


<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/repeatentries/#prc.entry.id#/edit')#" class="btn btn-primary">Edit</a>
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/repeatentries/#prc.entry.id#/delete')#" class="btn btn-danger">Delete</a>
    </div>
  </div>
</div>
</cfoutput>