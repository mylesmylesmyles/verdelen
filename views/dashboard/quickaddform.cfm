<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.errorMessages" type="array" default=[];
    param name="prc.entryTypeList" type="query";
    param name="prc.accounts" type="query";
  </cfscript>
</cfsilent>
<cfoutput>
<cfif prc.errorMessages.len()>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
    <ul>
      <cfscript>
        #prc.errorMessages.each(function (msg) {
          echo("<li>#msg#</li>")
        })#
      </cfscript>
    </ul>
  </div>
</cfif>
<form action="#event.buildLink('quickadd')#" method="post" class="form-horizontal">
  <div class="form-group form-group-lg">
    <label for="account" class="col-sm-3 control-label">Account</label>
    <div class="col-sm-9 input-group input-group-lg">
      <span class="input-group-addon"><span class="glyphicon glyphicon-inbox"></span></span>
      <cfparam name="rc.account" default=""/>
      <select name="account" id="account" required class="form-control">
        <option value="">Choose...</option>
        <cfloop query="#prc.accounts#">
        	<option value="#prc.accounts.id#"<cfif rc.account eq prc.accounts.id> selected</cfif>>#prc.accounts.name#</option>
        </cfloop>
      </select>
    </div>
  </div>
  <div class="form-group form-group-lg">
    <label for="amount" class="col-sm-3 control-label">Amount</label>
    <div class="col-sm-9 input-group input-group-lg">
      <span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
      <cfparam name="rc.amount" default=""/>
      <input type="number" step="0.01" id="amount" name="amount" placeholder="0.00" value="#isNumeric(rc.amount) ? rc.amount : ''#" required class="form-control"/>
    </div>
  </div>
 <div class="form-group form-group-lg">
    <label for="occurredAt" class="col-sm-3 control-label">On date</label>
    <div class="col-sm-9 input-group input-group-lg">
      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
      <cfparam name="rc.occurredAt" default="#dateFormat(now(), 'yyyy-mm-dd')#"/>
      <input type="date" id="occurredAt" name="occurredAt" placeholder="#dateFormat(now(), 'yyyy-mm-dd')#" value="#isDate(rc.occurredAt) ? rc.occurredAt : ''#" required class="form-control">
    </div>
  </div>
  <div class="form-group form-group-lg">
    <label for="type" class="col-sm-3 control-label">Action</label>
    <div class="col-sm-9 input-group input-group-lg">
      <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
      <cfparam name="rc.type" default=""/>
      <select name="type" id="type" required class="form-control">
        <option value="">Choose...</option>
        <cfloop query="#prc.entryTypeList#">
          <option value="#prc.entryTypeList.id#"<cfif rc.type eq prc.entryTypeList.id> selected</cfif>>#prc.entryTypeList.name#</option>
        </cfloop>
      </select>
    </div>
  </div>
  <div class="form-group form-group-lg">
    <label for="notes" class="col-sm-3 control-label">Notes</label>
    <div class="col-sm-9 input-group input-group-lg">
      <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
      <cfparam name="rc.notes" default=""/>
      <textarea name="notes" rows="5" class="form-control">#esapiEncode('html', rc.notes)#</textarea>
    </div>
  </div>
  <div class="row">
    <div class="text-center">
      <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-lg" value="Create entry"/>
    </div>
  </div>
</form>
</cfoutput>