<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

  param name="prc.account" type="any";
  param name="prc.entryList" type="any";
  param name="prc.entryTotal" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-10">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="#event.buildLink('accounts/#prc.account.id#')#">#esapiEncode('html', prc.account.name)#</a></li>
            <li class="active">Entries <span class="badge">#prc.entryTotal#</span></li>
          </ol>
        </div>
        <div class="col-sm-2 pull-right">
          <a href="#event.buildLink('accounts/#prc.account.id#/entries/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create entry</a>
        </div>
      </div>
    </div>
    <table class="table table-hover col-xs-10">
      <thead>
      	<tr>
        <th colspan="4">&nbsp;</th>
        <th class="hidden-xs" colspan="2">Created</th>
        <th class="hidden-xs" colspan="2">Modified</th>
  		<tr>
  			<th>&nbsp;</td>
        <th>Amount</th>
        <th>Occurred At</th>
        <th>Type</th>
        <th class="hidden-xs">On</th>
        <th class="hidden-xs">By</th>
        <th class="hidden-xs">On</th>
        <th class="hidden-xs">By</th>
  		</tr>
      </thead>
      <tbody>
        <cfloop query="#prc.entryList#">
        	<tr>
        		<td nowrap>
        			<a href="#event.buildLink('accounts/#prc.account.id#/entries/#prc.entryList.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span></a>
          		<a href="#event.buildLink('accounts/#prc.account.id#/entries/#prc.entryList.id#/delete')#" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </td>
            <td>#LSCurrencyFormat(prc.entryList.amount)#</td>
            <td>#dateFormat(prc.entryList.occurredAt, 'yyyy-mm-dd')#</td>
            <td>#prc.entryList.typename#</td>
            <td class="hidden-xs">#dateFormat(prc.entryList.createdAt, 'yyyy-mm-dd')#</td>
            <td class="hidden-xs">#esapiEncode('html', prc.entryList.createdByName)#</td>
            <td class="hidden-xs">#dateFormat(prc.entryList.modifiedAt, 'yyyy-mm-dd')#</td>
            <td class="hidden-xs">#esapiEncode('html', prc.entryList.modifiedByName)#</td>
        	</tr>
        </cfloop>
      </tbody>
    </table>
  </div>
</div>
<cfif prc.entryTotal gt 0>
  <cfmodule template="../paging.cfm" 
            offset="#rc.offset#" 
            max="#rc.max#" 
            recordCount="#prc.entryTotal#"
            url="#event.buildLink('accounts/#prc.account.id#/entries')#"/>
</cfif>
</cfoutput>