/**
*
* @file /handlers/Dashboard.cfc
* @author Myles Goodhue 
* @description  Main dashboard of the site
*
*/

component extends="coldbox.system.EventHandler"{

	this.allowedMethods = { 
        index = "GET,POST",
        list   = "GET",
        viewquickadd = "GET",
        savequickadd = "POST"
    };

    void function preHandler(event) {
	}

	// Default Action
	void function index(event,rc,prc) {
        prc.budget = entityNew('Budget').getCurrent(owner: prc.user);
        prc.entryTypeList = entityNew('EntryType').list();
        prc.accounts = entityNew('Account').list(criteria: {owner = prc.user})
        prc.last10Deductions = getModel('DashboardService').listLast10Entries(user: prc.user, entryType: entityNew('EntryType').get(2));
        prc.last10Contributions = getModel('DashboardService').listLast10Entries(user: prc.user, entryType: entityNew('EntryType').get(1));
    }

    void function viewquickadd(event,rc,prc) {
        prc.entryTypeList = entityNew('EntryType').list();
        prc.accounts = entityNew('Account').list(criteria: {owner = prc.user})
    }

    void function savequickadd(event,rc,prc) {
        event.paramValue( name='account', value='' );
        event.paramValue( name='amount', value='' );
        event.paramValue( name='occurredAt', value=now() );
        event.paramValue( name='type', value='' );
        event.paramValue( name='notes', value='' );
        
        var includeFields = ['amount,occurredAt,type,notes'];

        // Keep track of error messages
        var errorMessages = [];

        prc.account = entityNew('Account').get(rc.account, false);
        
        if (isNull(prc.account)) {
            errorMessages.append("You must select an account.");
        } else if (prc.account.budget.owner.id neq prc.user.id) {
            setNextEvent('403');
        }
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!isDate(rc.occurredAt)) {
            errorMessages.append("You must enter the date the entry occurred on.");
        }
        
        if (!isNumeric(rc.type)) {
            errorMessages.append("You must enter the entry type.");
        } else {
            var entryTypeObj = entityNew('EntryType').get(rc.type);
            if (isNull(entryTypeObj)) {
                errorMessages.append("The entry type selected is not valid.");
            }
        }
        
        if (notes.len() > 4000) {
            errorMessages.append("The notes must be less than 4000 characters.");
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList().listAppend('account     '));
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('quickadd');
            return;
        }
        
        transaction {
            var entry = entityNew('Entry').new({account: prc.account, createdby: prc.user, modifiedby: prc.user});
            entry.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: true,
                composeRelationships: true
            );
            entry.save(flush: true);
        }
        setNextEvent('accounts/#prc.account.id#/entries/#entry.id#');
    }
}