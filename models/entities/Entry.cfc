/**
*
* @file  /models/entities/Entry.cfc
* @author Myles Goodhue
* @description An account entry
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="Entry" 
			table="entries" {

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="account" 
				fieldtype="many-to-one" 
				cfc="Account" 
				fkcolumn="accounts_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.account = { required=true };

	property 	name="amount"
				column="amount"
				type="numeric"
				ormtype="float"
				precision=9
				scale=2
				sqltype="decimal(9,2)"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true;

	this.constraints.amount = { type="numeric", required=true };

	property 	name="occurredAt"
				column="occurred_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true;

	this.constraints.occurredAt = { type="date", required=true };

	property 	name="type" 
				fieldtype="many-to-one" 
				cfc="EntryType" 
				fkcolumn="entry_types_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.type = { required=true };

	property 	name="notes" 
				column="notes"
				type="string"
				sqltype="varchar(4000)"
				fieldtype="column"
				persistent=true
				required=false
				notnull=false;

	this.constraints.notes = { required=false };

	property 	name="createdAt"
				column="created_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true
				insert=false
				update=false;

	this.constraints.createdAt = { type="date", required=true };

	property 	name="createdBy" 
				fieldtype="many-to-one" 
				cfc="User" 
				fkcolumn="created_users_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.createdBy = { required=true };

	property 	name="modifiedAt"
				column="modified_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true;

	this.constraints.modifiedAt = { type="date", required=true };

	property 	name="modifiedBy" 
				fieldtype="many-to-one" 
				cfc="User" 
				fkcolumn="modified_users_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.modifiedBy = { required=true };

	property 	name="transfers"
				fieldtype="many-to-many"
				cfc="AccountTransfer"
				linktable="accounts_transfers_entries"
				fkcolumn="entries_id"
				inversejoincolumn="accounts_transfers_id"
				type="array"
				cascade="all"
				lazy=true
				batchsize="3";

	void function preInsert() {
		variables.createdAt = now();
		variables.modifiedAt = now();
	}

	void function preUpdate() {
		variables.modifiedAt = now();
	}

	/**
 	 * @override The postgres db doesn't support the current syntax used by the orm :(
	 */
	query function list(
			struct criteria=structnew(),
			string sortOrder="",
			numeric offset=0,
			numeric max=0,
			numeric timeout=0,
			boolean ignoreCase=false
		) {
		var qry = new Query();
		var sql = "
			with data as (
				select entries.id,
					entries.amount,
					entries.occurred_at as occurredAt,
					entries.entry_types_id as typeId,
					entry_types.name as typeName,
					entries.notes,
					entries.created_at as createdAt,
					entries.created_users_id as createdById,
					c_users.first_name || ' ' || c_users.last_name as createdByName,
					entries.modified_users_id as modifiedById,
					m_users.first_name || ' ' || m_users.last_name as modifiedByName,
					entries.modified_at as modifiedAt
				from entries
				join entry_types
					on entry_types.id = entries.entry_types_id
				join users c_users
					on entries.created_users_id = c_users.id
				join users m_users
					on entries.modified_users_id = m_users.id
				where 1=1
		";
		if (arguments.criteria.keyExists('account')) {
			sql &= " and entries.accounts_id = :account ";
			qry.addParam(name: 'account', value: arguments.criteria.account.id, cfsqltype: 'cf_sql_varchar');
		}
		sql &=" 
				order by occurred_at desc
			), counter as (
				select count(data.id) as total, 
					sum(case when data.typeId = 1 then data.amount else 0 end) - 
						sum(case when data.typeId = 2 then data.amount else 0 end) as balance
				from data
			) 
			select data.*, counter.total, counter.balance 
			from data, counter 
		";
		if (arguments.max > 0) {
			sql &= " limit :max ";
			qry.addParam(name: "max", value=arguments.max, cfsqltype: 'cf_sql_integer');
		} 
		if (arguments.offset > 0) {
			sql &= " offset :offset ";
			qry.addParam(name: "offset", value=arguments.offset, cfsqltype: 'cf_sql_integer');
		} 
		return qry.setSQL(sql).execute().getResult();
		
	}
}