/**
*
* @file  /handlers/Accounts.cfc
* @author Myles Goodhue 
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        view = "GET",
        list = "GET,POST",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE",
        listSharing = "GET",
        updateSharing = "PUT,POST"
    };

    void function preHandler(event) {
        var prc = event.getCollection(private: true);
        event.paramValue( name='budgetid', value='' );
        prc.budget = entityNew('Budget').getCurrent(owner: prc.user);
        if (isNull(prc.budget)) {
            setNextEvent('404');
        }
    }

    void function view(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.account = entityNew('Account').get(rc.id);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }
    }
    
    void function list(event,rc,prc) {
        event.paramValue( name='max', value='' );
        if (!isNumeric(rc.max) or rc.max eq 0) {
            rc.max = 10;
        }
        
        event.paramValue( name='offset', value='' );
        if (!isNumeric(rc.offset) or rc.offset <= 0) {
            rc.offset = 0;
        }
        
        prc.accountsList = entityNew('Account').list(criteria: {owner = prc.user}, offset: rc.offset, max: rc.max);
        prc.accountsTotal = isNumeric(prc.accountsList.total) ? prc.accountsList.total : 0;
        
    }
    void function update(event,rc,prc) {}
    
    void function createForm(event,rc,prc) {
        prc.xhSubmit = event.buildLink('accounts/new');
    }
    
    void function create(event,rc,prc) {
        // Input validation
        event.paramValue( name='name', value='' );
        if (!rc.name.trim().len()) {
            // Keep track of error messages
            var errorMessages = [];
            errorMessages.append("You must enter a name.");
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('accounts/new');
            return;
        }

        // Save the new budget
        transaction {
            // Merge the budget object into this session
            entityMerge(prc.budget);
            
            // Populate and save the 
            var account = entityNew('Account').new({name=rc.name.trim(), budget=prc.budget, isActive=true});
            account.save(flush: true);
        }

        setNextEvent('accounts/#account.id#');
    }
    
    void function verifyDelete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.account = entityNew('Account').get(rc.id);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }
    }
    
    void function delete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.account = entityNew('Account').get(rc.id);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }
    }
    
    void function listSharing(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.account = entityNew('Account').get(rc.id);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }
    }
    
    void function updateSharing(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.account = entityNew('Account').get(rc.id);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }

        setNextEvent('accounts/#account.id#/sharing');
    }

}