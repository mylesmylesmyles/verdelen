CREATE TABLE public.accounts
(
   id character varying(36) NOT NULL, 
   name character varying(30) NOT NULL, 
   budgets_id character varying(36) NOT NULL, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   CONSTRAINT accounts_pk PRIMARY KEY (id), 
   CONSTRAINT accounts_budgets_fk FOREIGN KEY (budgets_id) REFERENCES budgets (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
COMMENT ON COLUMN public.accounts.id IS 'The primary key';
COMMENT ON COLUMN public.accounts.name IS 'The account name';
COMMENT ON COLUMN public.accounts.budgets_id IS 'FK to BUDGETS.ID';
COMMENT ON COLUMN public.accounts.is_active IS 'Is the account currently active?';
COMMENT ON COLUMN public.accounts.modified_at IS 'The last time the account was modified';
COMMENT ON TABLE public.accounts
  IS 'Budget accounts';
