﻿component{

	// Configure ColdBox Application
	function configure(){

		// coldbox directives
		coldbox = {
			//Application Setup
			appName 				= "MySavingsTracker",
			eventName 				= "event",

			//Development Settings
			reinitPassword			= hash(rand()),
			handlersIndexAutoReload = false,

			//Implicit Events
			defaultEvent			= "Authorization.login",
			requestStartHandler		= "Main.onRequestStart",
			requestEndHandler		= "Main.onRequestEnd",
			applicationStartHandler = "Main.onAppInit",
			applicationEndHandler	= "Main.onApplicationEnd",
			sessionStartHandler 	= "Main.onSessionStart",
			sessionEndHandler		= "Main.onSessionEnd",
			missingTemplateHandler	= "Main.onMissingTemplate",

			//Extension Points
			applicationHelper 			= "includes/helpers/ApplicationHelper.cfm",
			viewsHelper					= "",
			modulesExternalLocation		= [],
			viewsExternalLocation		= "",
			layoutsExternalLocation 	= "",
			handlersExternalLocation  	= "",
			requestContextDecorator 	= "",
			controllerDecorator			= "",

			//Error/Exception Handling
			exceptionHandler		= "Main.onException",
			onInvalidEvent			= "Main.onInvalidEvent",
			customErrorTemplate		= "/error.cfm",

			//Application Aspects
			handlerCaching 			= true,
			eventCaching			= true,
			proxyReturnCollection 	= true
		};

		// custom settings
		settings = {};

		// environment settings, create a detectEnvironment() method to detect it yourself.
		// create a function with the name of the environment so it can be executed if that environment is detected
		// the value of the environment is a list of regex patterns to match the cgi.http_host.
		// environments = {
		// 	development = "localhost"
		// };

		// Module Directives
		modules = {
			//Turn to false in production
			autoReload = false,
			// An array of modules names to load, empty means all of them
			include = [],
			// An array of modules names to NOT load, empty means none
			exclude = []
		};

		//LogBox DSL
		logBox = {
			// Define Appenders
			appenders = {
				coldboxTracer = { class="coldbox.system.logging.appenders.ConsoleAppender" }
			},
			// Root Logger
			root = { levelmax="INFO", appenders="*" },
			// Implicit Level Categories
			info = [ "coldbox.system" ]
		};

		//Layout Settings
		layoutSettings = {
			defaultLayout = "",
			defaultView   = ""
		};

		//Interceptor Settings
		interceptorSettings = {
			throwOnInvalidStates = false,
			customInterceptionPoints = ""
		};

		//Register interceptors as an array, we need order
		interceptors = [
			//SES
			{ class="coldbox.system.interceptors.SES",
				properties={}
			},
			// Route authorization
			{ class="cbsecurity.interceptors.Security",
				name="CBSecurity",
				properties={
					rulesFile = "/app/config/Security.json.cfm",
					rulesSource = "json",
					validatorModel = "security.AuthorizationService"
				} 
			},
			{class="app.extensions.interceptors.PRCUser", properties={}}
		];

		/*
		// flash scope configuration
		flash = {
			scope = "session,client,cluster,ColdboxCache,or full path",
			properties = {}, // constructor properties for the flash scope implementation
			inflateToRC = true, // automatically inflate flash data into the RC scope
			inflateToPRC = false, // automatically inflate flash data into the PRC scope
			autoPurge = true, // automatically purge flash data for you
			autoSave = true // automatically save flash scopes at end of a request and on relocations.
		};

		//Register Layouts
		layouts = [
			{ name = "login",
		 	  file = "Layout.tester.cfm",
			  views = "vwLogin,test",
			  folders = "tags,pdf/single"
			}
		];

		//Conventions
		conventions = {
			handlersLocation = "handlers",
			viewsLocation 	 = "views",
			layoutsLocation  = "layouts",
			modelsLocation 	 = "models",
			eventAction 	 = "index"
		};

		//Datasources
		datasources = {
			mysite   = {name="mySite", dbType="mysql", username="root", password="pass"},
			blog_dsn = {name="myBlog", dbType="oracle", username="root", password="pass"}
		};
		*/
		
		// ORM services, injection, etc
		orm = {
			// entity injection
			injection = {
				// enable it
				enabled = true,
				// the include list for injection
				include = "",
				// the exclude list for injection
				exclude = ""
			}
		};

	}

	any function detectEnvironment() {

		if (cgi.http_host eq 'localhost') {
			return 'development';
		}
		
		if (cgi.http_host.left(9) eq '127.0.0.1') {
			return 'development';
		}

		return 'production';
	}
	
	

	/**
	* Development environment
	*/
	function development() {
		coldbox.reinitPassword = "";
		coldbox.handlersIndexAutoReload = true;
		coldbox.customErrorTemplate = "/coldbox/system/includes/BugReport.cfm";
		coldbox.missingTemplateHandler = "";
		coldbox.onInvalidEvent = "";
		coldbox.handlerCaching = false;
		coldbox.eventCaching = false;
		coldbox.proxyReturnCollection = false;
		
		// ColdBox Debugger Settings
		debugger = {
		    // Activate debugger for everybody
		    debugMode = true,
		    // Setup a password for the panel
		    debugPassword = "",
		    enableDumpVar = true,
		    persistentRequestProfiler = true,
		    maxPersistentRequestProfilers = 10,
		    maxRCPanelQueryRows = 50,
		    showTracerPanel = true,
		    expandedTracerPanel = true,
		    showInfoPanel = true,
		    expandedInfoPanel = true,
		    showCachePanel = true,
		    expandedCachePanel = false,
		    showRCPanel = true,
		    expandedRCPanel = false,
		    showModulesPanel = true,
		    expandedModulesPanel = false,
		    showRCSnapshots = false,
		    wireboxCreationProfiler=false
		};
	}

	// function afterConfigurationLoad() {
	//     var logBox = controller.getLogBox();
	//     logBox.registerAppender( 'tracer', 'cbdebugger.includes.appenders.ColdBoxTracerAppender' );
	//     var appenders = logBox.getAppendersMap( 'tracer' );
	    
	//     // Register the appender with the root loggger, and turn the logger on.
	//     var root = logBox.getRootLogger();
	//     root.addAppender( appenders['tracer'] );
	//     root.setLevelMax( 4 );
	//     root.setLevelMin( 0 );
	// }
}