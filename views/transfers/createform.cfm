<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.budget";
    param name="prc.errorMessages" type="array" default=[];
    param name="prc.entryFrequencyList" type="query";
    param name="prc.accountsList" type="query";

    param name="rc.deduct" default="0";
    param name="rc.repeat" default="0";
  </cfscript>
  <cfsavecontent variable="htmlHead">
    <script type="text/javascript">
      $(document).ready(function () {
         $('#repeat').change(function () {
          if ($(this).prop('checked')) {
            $('#repeatOptions').show();
            $('#frequency').prop('required', true);
            $('#frequencyCount').prop('required', true);
            return;
          } 
          $('#repeatOptions').hide();
          $('#frequency').prop('required', false);
          $('#frequencyCount').prop('required', false);
        }).change();
      });
    </script>
  </cfsavecontent>
  <cfhtmlhead text="#htmlhead#"/>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="well well-lg col-md-7 col-md-offset-2 col-xs-10 col-xs-offset-1">
    <h2 class="text-center">New account transfer</h2>
    <br>
    <cfif prc.errorMessages.len()>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
        <ul>
          <cfscript>
            #prc.errorMessages.each(function (msg) {
              echo("<li>#msg#</li>")
            })#
          </cfscript>
        </ul>
      </div>
    </cfif>
    <form action="#event.buildLink('transfers/new')#" method="post" class="form-horizontal">
      <div class="form-group form-group-lg">
        <label for="amount" class="col-sm-3 control-label">Amount</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
          <cfparam name="rc.amount" default=""/>
          <input type="number" step="0.01" id="amount" name="amount" placeholder="0.00" value="#isNumeric(rc.amount) ? rc.amount : ''#" required class="form-control"/>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="fromAccountID" class="col-sm-3 control-label">From Account</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
          <cfparam name="rc.fromAccountID" default=""/>
          <select name="fromAccountID" id="fromAccountID" required class="form-control">
            <option value="">Choose...</option>
            <cfloop query="#prc.accountsList#">
              <option value="#prc.accountsList.id#"<cfif rc.fromAccountID eq prc.accountsList.id> selected</cfif>>#prc.accountsList.name#</option>
            </cfloop>
          </select>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="toAccountId" class="col-sm-3 control-label">To Account</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
          <cfparam name="rc.toAccountId" default=""/>
          <select name="toAccountId" id="toAccountId" required class="form-control">
            <option value="">Choose...</option>
            <cfloop query="#prc.accountsList#">
              <option value="#prc.accountsList.id#"<cfif rc.toAccountId eq prc.accountsList.id> selected</cfif>>#prc.accountsList.name#</option>
            </cfloop>
          </select>
        </div>
      </div>

     <div class="form-group form-group-lg">
        <label for="frequencyStartAt" class="col-sm-3 control-label">On date</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          <cfparam name="rc.frequencyStartAt" default="#dateFormat(now(), 'yyyy-mm-dd')#"/>
          <input type="date" id="frequencyStartAt" name="frequencyStartAt" placeholder="#dateFormat(now(), 'yyyy-mm-dd')#" value="#isDate(rc.frequencyStartAt) ? rc.frequencyStartAt : ''#" required class="form-control">
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="deduct" class="col-sm-3 control-label checkbox">Deduct from target account</label>
        <div class="col-sm-9">
          <input type="checkbox" name="deduct" id="deduct" value="1"<cfif val(rc.deduct) eq 1> checked</cfif>/>
        </div>
      </div>

      <div class="form-group form-group-lg">
        <label for="repeat" class="col-sm-3 control-label checkbox">Repeat</label>
        <div class="col-sm-9">
          <input type="checkbox" name="repeat" id="repeat" value="1"<cfif val(rc.repeat) eq 1> checked</cfif>/>
        </div>
      </div>
      <div id="repeatOptions">
        <div class="form-group form-group-lg">
          <label for="type" class="col-sm-3 control-label">Frequency</label>
          <div class="col-sm-9 input-group input-group-lg">
            <span class="input-group-addon"><span class="glyphicon glyphicon-repeat"></span></span>
            <cfparam name="rc.frequency" default=""/>
            <select name="frequency" id="frequency" required class="form-control">
              <option value="">Choose...</option>
              <cfloop query="#prc.entryFrequencyList#">
                <option value="#prc.entryFrequencyList.id#"<cfif rc.frequency eq prc.entryFrequencyList.id> selected</cfif>>#prc.entryFrequencyList.name#</option>
              </cfloop>
            </select>
          </div>
        </div>
        <div class="form-group form-group-lg">
          <label for="frequencyCount" class="col-sm-3 control-label">Count</label>
          <div class="col-sm-9 input-group input-group-lg">
            <span class="input-group-addon"><span class="glyphicon glyphicon-forward"></span></span>
            <cfparam name="rc.frequencyCount" default=""/>
            <input type="text" id="frequencyCount" name="frequencyCount" placeholder="1" value="#isNumeric(rc.frequencyCount) ? rc.frequencyCount : ''#" required class="form-control" aria-label=""/>
          </div>
        </div>

       <div class="form-group form-group-lg">
          <label for="frequencyEndAt" class="col-sm-3 control-label">End date</label>
          <div class="col-sm-9 input-group input-group-lg">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            <cfparam name="rc.frequencyEndAt" default=""/>
            <input type="date" id="frequencyEndAt" name="frequencyEndAt" placeholder="#dateFormat(dateAdd("d", 1, now()), 'yyyy-mm-dd')#" value="#isDate(rc.frequencyEndAt) ? rc.frequencyEndAt : ''#" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="notes" class="col-sm-3 control-label">Notes</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
          <cfparam name="rc.notes" default=""/>
          <textarea name="notes" rows="5" class="form-control">#esapiEncode('html', rc.notes)#</textarea>
        </div>
      </div>
      <div class="row">
        <div class="text-center">
          <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-lg" value="Create entry"/>
        </div>
      </div>
    </form>
  </div>
</div>
<script src="/#getSetting('AppMapping')#/includes/js/handlers/transfers/create.js" type="text/javascript" async></script>
</cfoutput>