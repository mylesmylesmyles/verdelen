﻿<cfscript>
  param name="prc" type="struct";
  param name="prc.user";
</cfscript>
<cfoutput>
	<div class="row large-collapse">
	  	<h3 class="text-center">Hi #esapiEncode('html', prc.user.firstname)#!</h3 class="text-center">
	</div>

<div class="row">
	<div class="clearfix"><br/></div>
</div>

<div class="row visible-xs-block">
	<div class="col-xs-12">
  <a href="#event.buildLink("quickadd")#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Create Entry</a>
	</div>
</div>

<div class="row visible-xs-block">
	<div class="col-xs-12">
	<a href="#event.buildLink('accounts/')#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List accounts</a>
	</div>
</div>

<div class="row">
	<div class="col-md-4">
	    <h4><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Total accounts:
		    <em class="text-right"><a href="#event.buildLink('accounts/')#">#prc.budget.getTotalAccounts()#</a></em></h4>
	</div>
	<div class="col-md-4">
		    <h4><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Entries:
		    <em>#prc.budget.getTotalEntries()#</em></h4>
	</div>
	<div class="col-md-4">
		    <h4><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> Balance:
		    <em>#LSCurrencyFormat(prc.budget.getBalance())#</em></h4>
	</div>
</div>

<div class="row hidden-xs">
	<div class="clearfix"><br/><div>
</div>

<div class="row hidden-xs">
	<div class="col-sm-6">
	<a href="#event.buildLink('accounts/')#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List accounts</a>
	</div>
	<div class="col-sm-6">
	<a href="#event.buildLink("accounts/new")#" class="btn btn-info btn-block btn-lg col-xs-12" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create account</a>
	</div>
</div>

<div class="row hidden-xs">
	<div class="clearfix"><br/></div>
</div>

<div class="row hidden-xs">
	<div class="col-sm-6">
	<a href="#event.buildLink('transfers/')#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List transfers</a>
	</div>
	<div class="col-sm-6">
	<a href="#event.buildLink('transfers/new')#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Transfer funds</a>
	</div>
</div>

<div class="row hidden-xs">
	<div class="clearfix"><br/><div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<cfif prc.last10Deductions.recordCount>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="last10deductions">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="##accordion" href="##collapseLast10deductions" aria-expanded="true" aria-controls="collapseLast10deductions">
								Last 10 deductions
							</a>
						</h4>
					</div>
				</div>
				<div id="collapseLast10deductions" class="panel-collapse collapse" role="tabpanel" aria-labelledby="last10deductions">
					<div class="panel-body">
						<table class="table table-hover">
					      <thead>
					      	<tr>
					      		<th>&nbsp;</th>
						        <th>Amount</th>
						        <th>Occurred At</th>
						        <th>Account</th>
						    </tr>
					      </thead>
					      <tbody>
					        <cfloop query="#prc.last10Deductions#">
					        	<tr>
					        		<td nowrap>
					        			<a href="#event.buildLink('accounts/#prc.last10Deductions.accounts_id#/entries/#prc.last10Deductions.entries_id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span></a>
					          		<a href="#event.buildLink('accounts/#prc.last10Deductions.accounts_id#/entries/#prc.last10Deductions.entries_id#/delete')#" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
						            </td>
						            <td>#LSCurrencyFormat(prc.last10Deductions.amount)#</td>
						            <td>#dateFormat(prc.last10Deductions.occurredAt, 'yyyy-mm-dd')#</td>
						            <td><a href="#event.buildLink('accounts/#prc.last10Deductions.accounts_id#')#">#esapiEncode('html', prc.last10Deductions.accounts_name)#</a></td>
					            </tr>
					        </cfloop>
					      </tbody>
					    </table>
					</div>
				</div>
			</cfif>
			<cfif prc.last10Contributions.recordCount>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="last10contributions">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="##accordion" href="##collapseLast10contributions" aria-expanded="false" aria-controls="collapseLast10contributions">
								Last 10 contributions
							</a>
						</h4>
					</div>
				</div>
				<div id="collapseLast10contributions" class="panel-collapse collapse" role="tabpanel" aria-labelledby="last10contributions">
					<div class="panel-body">
						 <table class="table table-hover col-xs-6">
					      <thead>
					      	<tr>
					      		<th>&nbsp;</th>
						        <th>Amount</th>
						        <th>Occurred At</th>
						        <th>Account</th>
						    </tr>
					      </thead>
					      <tbody>
					        <cfloop query="#prc.last10Contributions#">
					        	<tr>
					        		<td nowrap>
					        			<a href="#event.buildLink('accounts/#prc.last10Contributions.accounts_id#/entries/#prc.last10Contributions.entries_id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span></a>
					          		<a href="#event.buildLink('accounts/#prc.last10Contributions.accounts_id#/entries/#prc.last10Contributions.entries_id#/delete')#" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
						            </td>
						            <td>#LSCurrencyFormat(prc.last10Contributions.amount)#</td>
						            <td>#dateFormat(prc.last10Contributions.occurredAt, 'yyyy-mm-dd')#</td>
						            <td><a href="#event.buildLink('accounts/#prc.last10Contributions.accounts_id#')#">#esapiEncode('html', prc.last10Contributions.accounts_name)#</a></td>
					            </tr>
					        </cfloop>
					      </tbody>
					    </table>
					</div>
				</div>
			</cfif>
			<cfif prc.accounts.recordCount>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="createnewentry">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="##accordion" href="##collapsecreatenewentry" aria-expanded="false" aria-controls="collapsecreatenewentry">
								Create a new account entry
							</a>
						</h4>
					</div>
				</div>
				<div id="collapsecreatenewentry" class="panel-collapse collapse" role="tabpanel" aria-labelledby="createnewentry">
					<div class="panel-body">
						<cfinclude template="quickaddform.cfm"/>
					</div>
				</div>
			</cfif>
		</div>
	</div>
</div>
</cfoutput>
