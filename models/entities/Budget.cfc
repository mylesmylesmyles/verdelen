/**
*
* @file  /models/entities/Budget.cfc
* @author Myles Goodhue
* @description A budget entity
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="Budget" 
			table="budgets"
			autowire=true 
			accessors=true {

	property 	name="util"
				persistent=false
				inject="model:util";

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="name"
				column="name"
				type="string"
				sqltype="varchar(30)"
				fieldtype="column"
				persistent=true
				length="30"
				required=true;

	this.constraints.name = { type="string", required=true, length="1..30" };

	property 	name="isActive"
				column="is_active"
				type="boolean"
				sqltype="boolean"
				fieldtype="column"
				persistent=true
				required=true
				dbdefault=true;

	this.constraints.isActive = { type="boolean", required=true };

	property 	name="createdAt"
				column="created_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true
				insert=false
				update=false;

	this.constraints.createdAt = { type="date", required=true };

	property 	name="owner" 
				fieldtype="many-to-one" 
				cfc="User" 
				fkcolumn="owner_users_id" 
				column="id" 
				cascade="delete" 
				lazy=true
				notnull=true;

	this.constraints.owner = { required=true };

	void function preInsert() {
		variables.createdAt = now();
	}

	any function getCurrent (required User owner) {
		return entityNew('Budget').newCriteria()
            .eq("owner", arguments.owner)
            .isTrue("isActive")
            .list()
            .get(0)
	}

	/**
 	 * @hint Find all budgets associated to this user
	 */
	// query function listByOwner(required User owner, numeric offset=0, numeric max=0) {
	// 	var connection = ormGetSession().connection();
	// 	var stmt = connection.prepareStatement("
	// 		select id, name, is_active as isActive, created_at as createdAt 
	// 		from budgets 
	// 		where owner_users_id = ?
	// 	");
	// 	stmt.setString(1, arguments.owner.id);
	// 	if (arguments.max > 0) {
	// 		stmt.setMaxRows(javaCast('int', arguments.max));
	// 	}
	// 	stmt.executeQuery();
	// 	return util.resultsetToQuery(stmt.getResultSet());
	// }

	/**
 	 * @hint Get the current balanace for all transactions
	 */
	numeric function getBalance(date onDate=now()) {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			select sum(case when entries.entry_types_id = 2 then -entries.amount else entries.amount end ) as balance
			from budgets
			inner join accounts
				on accounts.budgets_id = budgets.id
			inner join entries
				on entries.accounts_id = accounts.id
				and date_trunc('day', entries.occurred_at) <= cast('" & dateFormat(arguments.onDate, 'yyyy-mm-dd') & "' as timestamp)
			where budgets.id = ?
		");
		stmt.setString(1, variables.id);
		stmt.executeQuery();
		return util.resultsetToQuery(stmt.getResultSet()).balance;
	}

	/**
 	 * @hint Get the total number of transactions
	 */
	numeric function getTotalEntries() {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			select count(entries.id) as total
			from budgets
			inner join accounts
				on accounts.budgets_id = budgets.id
			inner join entries
				on entries.accounts_id = accounts.id
			where budgets.id = ?
		");
		stmt.setString(1, variables.id);
		stmt.executeQuery();
		return util.resultsetToQuery(stmt.getResultSet()).total;
	}

	/**
 	 * @hint Get the total number of transactions
	 */
	any function getLastEntry() {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			select entries.id
			from budgets
			inner join accounts
				on accounts.budgets_id = budgets.id
			inner join entries
				on entries.accounts_id = accounts.id
			where budgets.id = ?
			order by entries.modified_at desc
			limit 1
		");
		stmt.setString(1, variables.id);
		stmt.executeQuery();
		var id = util.resultsetToQuery(stmt.getResultSet()).id;
		if (!isNull(id)) {
			return entityeNew('Entry').get(id);
		}
	}

	/**
 	 * @hint Get the total number of accounts in this budget
	 */
	numeric	 function getTotalAccounts() {
		return entityNew('Account').newCriteria()
            .createAlias("budget","b")
                .eq("b.id", variables.id)
            .count();
	}
}