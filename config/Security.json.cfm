[
    {
        "whitelist": "^authorization.*,^main.*,^schedule",
        "securelist": ".*",
        "match": "event",
        "roles": "user,admin",
        "permissions": "",
        "redirect": "login",
        "useSSL": false
    }
]