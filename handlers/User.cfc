/**
*
* @file  /handlers/User.cfc
* @author Myles Goodhue 
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        profile = "GET",
        view = "GET",
        list = "GET,POST",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE",
        updatePassword = "POST",
        edit = "GET"
    };

    void function preHandler(event) {
    }

    void function profile(event,rc,prc) {
        // Submit exit handler
        prc.xhChangePassword = event.buildLink('users/myprofile/password');
    }
    void function view(event,rc,prc) {
        event.paramValue('id', '');
        prc.userAccount = entityNew('User').get(id: rc.id, returnNew: false);
        if (isNull(prc.userAccount)) {
            setNextEvent('404');
        }
    }

    void function list(event,rc,prc) {
        event.paramValue( name='max', value='' );
        if (!isNumeric(rc.max) or rc.max eq 0) {
            rc.max = 10;
        }
        
        event.paramValue( name='offset', value='' );
        if (!isNumeric(rc.offset) or rc.offset <= 0) {
            rc.offset = 0;
        }
        
        prc.usersList = entityNew('User').list(criteria: {}, offset: rc.offset, max: rc.max);
        prc.usersTotal = prc.usersList.total;
    }
    
    void function edit(event,rc,prc) {
        event.paramValue('id', '');
        prc.userAccount = entityNew('User').get(id: rc.id, returnNew: false);
        if (isNull(prc.userAccount)) {
            setNextEvent('404');
        }
        prc.xhSubmit = event.buildLink('users/#prc.userAccount.getID()#');
    }
    
    void function update(event,rc,prc) {
        var includeFields = ['firstName,lastName,email,isActive'];
        event.paramValue('id', '');
        var userAccount = entityNew('User').get(id: rc.id, returnNew: false);
        if (isNull(userAccount)) {
            setNextEvent('404');
        }
        event.paramValue( name='firstName', value=userAccount.getfirstName() );
        event.paramValue( name='lastName', value=userAccount.getlastName() );
        event.paramValue( name='email', value=userAccount.getemail() );
        event.paramValue( name='isActive', value=userAccount.getisActive() );
        
        // Keep track of error messages
        var errorMessages = [];

        if (!rc.firstName.trim().len()) {
            errorMessages.append("You must enter a first name");
        } else if (rc.firstName.trim().len() > 30) {
            errorMessages.append("The first name must be less than or equal to 30 characters");
        }

        if (!rc.lastName.trim().len()) {
            errorMessages.append("You must enter a last name");
        } else if (rc.lastName.trim().len() > 30) {
            errorMessages.append("The last name must be less than or equal to 30 characters");
        }

        if (!isBoolean(rc.isActive)) {
            errorMessages.append("Invalid active flag");
        } 

        if (!rc.email.trim().len()) {
            errorMessages.append("You must enter a login email");
        } else if (rc.email.trim().len() > 320) {
            errorMessages.append("The last name must be less than or equal to 320 characters");
        } else {
            var testUser = entityNew('User').findWhere(criteria={email: rc.email});
            if (!isNull(testUser) and testUser.getId() neq userAccount.getId()) {
                errorMessages.append("The new email address belongs to another account.");
            }
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('users/#userAccount.getId()#/edit');
            return;
        }

        transaction {

            userAccount.populate( 
                memento: rc, 
                trustedSetter: true, 
                include: includeFields.toList(), 
                ignoreEmpty: true, 
                nullEmptyInclude: includeFields.toList() 
            );
            userAccount.save(flush: true);
        }

        setNextEvent('users/#userAccount.getID()#');
    }
    
    void function createForm(event,rc,prc) {
        prc.xhSubmit = event.buildLink('users/new');
    }

    void function create(event,rc,prc) {
        var includeFields = ['firstName,lastName,email,password,lastlogin'];
        event.paramValue( name='firstName', value='' );
        event.paramValue( name='lastName', value='' );
        event.paramValue( name='email', value='' );
        event.paramValue( name='password', value='' );
        
        // Keep track of error messages
        var errorMessages = [];

        if (!rc.firstName.trim().len()) {
            errorMessages.append("You must enter a first name");
        } else if (rc.firstName.trim().len() > 30) {
            errorMessages.append("The first name must be less than or equal to 30 characters");
        }

        if (!rc.lastName.trim().len()) {
            errorMessages.append("You must enter a last name");
        } else if (rc.lastName.trim().len() > 30) {
            errorMessages.append("The last name must be less than or equal to 30 characters");
        }

        if (!rc.email.trim().len()) {
            errorMessages.append("You must enter a login email");
        } else if (rc.email.trim().len() > 320) {
            errorMessages.append("The last name must be less than or equal to 320 characters");
        } else {
            var testUser = entityNew('User').findWhere(criteria={email: rc.email});
            if (!isNull(testUser)) {
                errorMessages.append("The new email address belongs to another account.");
            }
        }

        if (!rc.password.trim().len()) {
            errorMessages.append("You must enter a password");
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('users/new');
            return;
        }

        transaction {
            
            var newUser = EntityNew('User').new({isActive: true, admin: false});

            newUser.populate( 
                memento: rc, 
                trustedSetter: true, 
                include: includeFields.toList(), 
                ignoreEmpty: true, 
                nullEmptyInclude: includeFields.toList() 
            );
            newUser.save(flush: true);


            // Create the default budget
            entityNew('Budget')
                .new({name: 'Default', owner: newUser, isactive: true})
                .save(flush: true);
        }

        setNextEvent('users/#newUser.getID()#');
    }

    void function verifyDelete(event,rc,prc) {
        event.paramValue('id', '');
        prc.userAccount = entityNew('User').get(id: rc.id, returnNew: false);
        if (isNull(prc.userAccount)) {
            setNextEvent('404');
        }
    }
    
    void function delete(event,rc,prc) {
        event.paramValue('id', '');
        var userAccount = entityNew('User').get(id: rc.id, returnNew: false);
        if (isNull(userAccount)) {
            setNextEvent('404');
        }
        transaction {
            userAccount.setIsActive(false);
            userAccount.save(flush:true);
        }
        setNextEvent('users/#userAccount.getID()#');
    }

    void function updatePassword(event,rc,prc) {
        event.paramValue( name='user', value='', private=true );
        event.paramValue( name='password', value='' );
        event.paramValue( name='new_password', value='' );
        event.paramValue( name='new_password_confirm', value='' );
        
        // Keep track of error messages
        var errorMessages = [];

        // Input validation
        if (!rc.password.trim().len()) {
            errorMessages.append("You must enter your current password.");
        }

        if (!event.valueExists( name='user', private=true )) {
            errorMessages.append("Unable to verify who you are!  This really shouldn't happen.");
        } else if (rc.password.trim().len() and !prc.user.isValidPassword(rc.password.trim())) {
            errorMessages.append("The credentials you have entered are invalid.");
        }

        if (!rc.new_password.trim().len() or rc.new_password neq rc.new_password_confirm) {
            errorMessages.append("The new password and confirmed password do not match");
        }
    
        if (errorMessages.len()) {
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('/users/myprofile');
        }

        transaction {
            entityMerge(prc.user);
            prc.user.setPassword(rc.new_password);
            prc.user.save();
        }

        flash.put( name="successMessage", value="Your password has been updated", inflateToPRC=true, autoPurge=true );
            
        setNextEvent('/users/myprofile');
    }

}