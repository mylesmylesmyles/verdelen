 /**
*
* @file  /models/entities/AccountTransfer.cfc
* @author  
* @description
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="AccountTransfer" 
			table="accounts_transfers" {

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="amount"
				column="amount"
				type="numeric"
				ormtype="float"
				precision=6
				scale=2
				sqltype="decimal(6,2)"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true;

	this.constraints.amount = { type="numeric", required=true };

	property 	name="fromAccount" 
				fieldtype="many-to-one" 
				cfc="Account" 
				fkcolumn="from_accounts_id" 
				column="id" 
				cascade="delete" 
				lazy=true 
				notnull=true;

	this.constraints.fromAccount = { required=true };

	property 	name="toAccount" 
				fieldtype="many-to-one" 
				cfc="Account" 
				fkcolumn="to_accounts_id" 
				column="id" 
				cascade="delete" 
				lazy=true 
				notnull=true;

	this.constraints.toAccount = { required=true };

	property 	name="frequency" 
				fieldtype="many-to-one" 
				cfc="EntryFrequency" 
				fkcolumn="entry_frequency_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.frequency = { required=true };

	property 	name="frequencyCount"
				column="entry_frequencies_count"
				type="numeric"
				sqltype="integer"
				fieldtype="column"
				persistent=true
				required=false
				notnull=false
				dbdefault=1;

	this.constraints.frequencyCount = { type="numeric", required=false };

	property 	name="frequencyStartAt"
				column="entry_frequencies_start_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=false
				notnull=false;

	this.constraints.frequencyStartAt = { type="date", required=true };

	property 	name="frequencyEndAt"
				column="entry_frequencies_end_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=false
				notnull=false;

	this.constraints.frequencyEndAt = { type="date", required=true };

	property 	name="notes" 
				column="notes"
				type="string"
				sqltype="varchar(4000)"
				fieldtype="column"
				persistent=true
				required=false
				notnull=false;

	this.constraints.notes = { required=true };

	property 	name="createdAt"
				column="created_at"
				type="date"
				sqltype="timestamp"
				fieldtypcolumn="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true
				insert=false
				update=false;

	this.constraints.createdAt = { type="date", required=true };

	property 	name="createdBy" 
				fieldtype="many-to-one" 
				cfc="User" 
				fkcolumn="created_users_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.createdBy = { required=true };

	property 	name="modifiedAt"
				column="modified_at"
				type="date"
				sqltype="timestamp"
				fieldtypcolumn="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true;

	this.constraints.modifiedAt = { type="date", required=true };

	property 	name="modifiedBy" 
				fieldtype="many-to-one" 
				cfc="User" 
				fkcolumn="modified_users_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.modifiedBy = { required=true };

	property 	name="fromRepeatEntry" 
				fieldtype="many-to-one" 
				cfc="RepeatEntry" 
				fkcolumn="from_repeat_entries_id" 
				column="id" 
				cascade="delete" 
				lazy=true 
				notnull=false;

	this.constraints.fromRepeatEntry = { required=false };

	property 	name="toRepeatEntry" 
				fieldtype="many-to-one" 
				cfc="RepeatEntry" 
				fkcolumn="to_repeat_entries_id" 
				column="id" 
				cascade="delete" 
				lazy=true 
				notnull=false;

	this.constraints.toRepeatEntry = { required=false };

	property 	name="deductRepeatEntry" 
				fieldtype="many-to-one" 
				cfc="RepeatEntry" 
				fkcolumn="deduct_repeat_entries_id" 
				column="id" 
				cascade="delete" 
				lazy=true 
				notnull=false;

	this.constraints.toRepeatEntry = { required=false };

	property 	name="entries"
				singularname="entry"
				fieldtype="many-to-many"
				cfc="Entry"
				linktable="accounts_transfers_entries"
				fkcolumn="accounts_transfers_id"
				inversejoincolumn="entries_id"
				type="array"
				cascade="all"
				lazy=true
				batchsize="3";

	void function preInsert() {
		variables.createdAt = now();
		variables.modifiedAt = now();
	}

	void function preUpdate() {
		variables.modifiedAt = now();
	}
	
	any function criteriaForBudget(required Budget budget) {
		var c = entityNew('AccountTransfer').newCriteria();
        var fdc = c.createSubcriteria( 'Account', 'FAccount' )
          .withProjections( property='id' )
          .isEq( 'budget.id', arguments.budget.id );
        var tdc = c.createSubcriteria( 'Account', 'TAccount' )
          .withProjections( property='id' )
          .isEq( 'budget.id', arguments.budget.id );
        c.and(fdc.propertyIn('fromAccount.id'),tdc.propertyIn('toAccount.id'));
        return c;
	}
	
	/**
 	 * @override The postgres db doesn't support the current syntax used by the orm :(
	 */
	query function list(
			struct criteria=structnew(),
			string sortOrder="",
			numeric offset=0,
			numeric max=0,
			numeric timeout=0,
			boolean ignoreCase=false
		) {
		var qry = new Query();
		var sql = "
			with data as (
				select at.id, 
					at.amount, 
					at.from_accounts_id fromaccount_id,
					from_accounts.name fromaccount_name,
					at.to_accounts_id toaccount_id,
					to_accounts.name toaccount_name,
					at.notes,
					at.created_at createdAt,
					at.created_users_id createdById,
					c_users.first_name || ' ' || c_users.last_name as createdByName,
					at.modified_at modifiedAt,
					at.modified_users_id modifiedById,
					m_users.first_name || ' ' || m_users.last_name as modifiedByName
				from accounts_transfers at
				join accounts from_accounts
					on from_accounts.id = at.from_accounts_id
				join accounts to_accounts
					on to_accounts.id = at.to_accounts_id
				join users c_users
					on at.created_users_id = c_users.id
				join users m_users
					on at.modified_users_id = m_users.id
				where 1=1
		";

		if (arguments.criteria.keyExists('budget')) {
			sql &= " and :budget in (from_accounts.budgets_id, to_accounts.budgets_id)";
			qry.addParam(name: 'budget', value: arguments.criteria.budget.id, cfsqltype: 'cf_sql_varchar');
		}
		if (arguments.criteria.keyExists('from_account')) {
			sql &= " and at.from_accounts_id = :from_account ";
			qry.addParam(name: 'from_account', value: arguments.criteria.from_account.id, cfsqltype: 'cf_sql_varchar');
		}
		if (arguments.criteria.keyExists('to_account')) {
			sql &= " and at.to_accounts_id = :to_account ";
			qry.addParam(name: 'to_account', value: arguments.criteria.to_account.id, cfsqltype: 'cf_sql_varchar');
		}
		sql &=" 
				order by created_at desc
			), counter as (
				select count(data.id) as total
				from data
			) 
			select data.*, counter.total 
			from data, counter 
		";
		if (arguments.max > 0) {
			sql &= " limit :max ";
			qry.addParam(name: "max", value=arguments.max, cfsqltype: 'cf_sql_integer');
		} 
		if (arguments.offset > 0) {
			sql &= " offset :offset ";
			qry.addParam(name: "offset", value=arguments.offset, cfsqltype: 'cf_sql_integer');
		} 
		return qry.setSQL(sql).execute().getResult();
		
	}
}