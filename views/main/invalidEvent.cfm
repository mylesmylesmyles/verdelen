<div class="row">
  <div class="text-center small-12 columns">
	<div data-alert class="alert-box warning round text-center">
		<strong>Sorry.  I couldn't find what you were looking for.</strong>
	</div>
  </div>
</div>