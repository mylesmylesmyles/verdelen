/**
*
* @file  /handlers/Transfers.cfc
* @author Myles Goodhue 
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        view = "GET",
        list = "GET,POST",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE"
    };

    void function preHandler(event) {
        prc.budget = entityNew('Budget').getCurrent(owner: prc.user);
        if (isNull(prc.budget)) {
            setNextEvent('404');
        }
    }

    void function view(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.transfer = entityNew('AccountTransfer').get(rc.id);
        if (isNull(prc.transfer.id)) {
            setNextEvent('404');
        }
    }
    
    void function list(event,rc,prc) {
        var qryArgs = {
            max: 10,
            offset: 0
        };
        event.paramValue( name='max', value='' );
        if (!isNumeric(rc.max) or rc.max eq 0) {
            rc.max = 10;
        }
        qryArgs.max = rc.max;
        
        event.paramValue( name='offset', value='' );
        if (!isNumeric(rc.offset) or rc.offset <= 0) {
            rc.offset = 0;
        }
        qryArgs.offset = rc.offset;
        qryArgs.budget = prc.budget;
        
        prc.tranfersList = entityNew('AccountTransfer').list(argumentCollection: qryArgs);
    }

    void function update(event,rc,prc) {}
    
    void function createForm(event,rc,prc) {
        prc.entryFrequencyList = entityNew('EntryFrequency').list();
        prc.accountsList = entityNew('Account').newCriteria()
            .createAlias("budget","b")
            .eq("b.owner", prc.user)
            .order('name', 'asc')
            .list(asQuery: true);
    }
    
    void function create(event,rc,prc) {
        
        event.paramValue( name='amount', value='' );
        event.paramValue( name='fromAccountId', value='' );
        event.paramValue( name='toAccountId', value='' );
        event.paramValue( name='type', value='' );
        event.paramValue( name='repeat', value=0 );
        event.paramValue( name='deduct', value=0 );
        event.paramValue( name='frequency', value='' );
        event.paramValue( name='frequencyCount', value='' );
        event.paramValue( name='frequencyStartAt', value='' );
        event.paramValue( name='frequencyEndAt', value='' );
        event.paramValue( name='notes', value='' );
        
        var includeFields = ['amount,notes,frequencyStartAt'];

        // Keep track of error messages
        var errorMessages = [];
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!rc.fromAccountId.len()) {
            errorMessages.append("You must enter a 'from' account.");
        } else {
            rc.fromAccount = entityNew('Account').get(rc.fromAccountId);
            if (isNull(rc.fromAccount.id) or rc.fromAccount.budget.id neq prc.budget.id) {
                errorMessages.append("The 'from' account selected is not valid.");
            }
        }
        
        if (!rc.toAccountId.len()) {
            errorMessages.append("You must enter a 'to' account.");
        } else {
            rc.toAccount = entityNew('Account').get(rc.toAccountId);
            if (isNull(rc.toAccount.id) or rc.toAccount.budget.id neq prc.budget.id) {
                errorMessages.append("The 'to' account selected is not valid.");
            }
        }

        if (!isDate(rc.frequencyStartAt)) {
            errorMessages.append("You must enter the start date of the entries.");
        }

        if (val(rc.repeat) eq 1) {

            includeFields.append('frequency');
            includeFields.append('frequencyCount');
            includeFields.append('frequencyEndAt');

            if (!isNumeric(rc.frequency)) {
                errorMessages.append("You must select a repeat frequency.");
            } else {
                var frequencyObj = entityNew('EntryFrequency').get(rc.frequency);
                if (isNull(frequencyObj.id)) {
                    errorMessages.append("The repeat frequency selected is not valid.");
                }
            }

            if (!isNumeric(rc.frequencyCount) or rc.frequencyCount < 0) {
                errorMessages.append("You must enter a frequency count greater than or equal to zero.");
            }

            if (isDate(rc.frequencyEndAt)) {
                if (isDate(rc.frequencyStartAt) and dateCompare(rc.frequencyStartAt, rc.frequencyEndAt, 'd') eq 1) {
                    errorMessages.append("The frequency start date must be before the frequency end date.");
                }
                includeFields.append('frequencyEndAt');
            }   
        }

        if (errorMessages.len()) {
            includeFields.append('repeat');
            includeFields.append('fromAccountId');
            includeFields.append('toAccountId');
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('transfers/new');
            return;
        }

        transaction {
            var transfer = entityNew('AccountTransfer').new({isActive: true, createdby: prc.user, modifiedby: prc.user});

            var fromEntry = entityNew('Entry').new();
            fromEntry.account = rc.fromAccount;
            fromEntry.amount = rc.amount;
            fromEntry.occurredAt = rc.frequencyStartAt;
            fromEntry.type = entityNew('EntryType').get(2);
            fromEntry.createdBy = prc.user;
            fromEntry.modifiedBy = prc.user;
            fromEntry.notes = "Transfer to #rc.toAccount.name#";
            fromEntry.save();
            transfer.addEntry(fromEntry);

            if (val(rc.repeat) eq 1) {
                var fromRepeatEntry = entityNew('RepeatEntry').new();
                fromRepeatEntry.account = rc.fromAccount;
                fromRepeatEntry.amount = rc.amount;
                fromRepeatEntry.type = entityNew('EntryType').get(2);
                fromRepeatEntry.frequency = entityNew('EntryFrequency').get(rc.frequency);
                fromRepeatEntry.frequencyCount = rc.frequencyCount;
                fromRepeatEntry.frequencyStartAt = rc.frequencyStartAt;
                if (isDate(rc.frequencyEndAt)) {
                    fromRepeatEntry.frequencyEndAt = rc.frequencyEndAt;
                }
                fromRepeatEntry.createdBy = prc.user;
                fromRepeatEntry.modifiedBy = prc.user;
                fromRepeatEntry.notes = "Scheduled transfer to #rc.toAccount.name#";
                fromRepeatEntry.save();
                transfer.fromRepeatEntry = fromRepeatEntry;
            }

            var toEntry = entityNew('Entry').new();
            toEntry.account = rc.toAccount;
            toEntry.amount = rc.amount;
            toEntry.occurredAt = rc.frequencyStartAt;
            toEntry.type = entityNew('EntryType').get(1);
            toEntry.createdBy = prc.user;
            toEntry.modifiedBy = prc.user;
            toEntry.notes = "Transfer from #rc.fromAccount.name#";
            toEntry.save();
            transfer.addEntry(toEntry);

            if (val(rc.repeat) eq 1) {
                var toRepeatEntry = entityNew('RepeatEntry').new();
                toRepeatEntry.account = rc.toAccount;
                toRepeatEntry.amount = rc.amount;
                toRepeatEntry.type = entityNew('EntryType').get(1);
                toRepeatEntry.frequency = entityNew('EntryFrequency').get(rc.frequency);
                toRepeatEntry.frequencyCount = rc.frequencyCount;
                toRepeatEntry.frequencyStartAt = rc.frequencyStartAt;
                if (isDate(rc.frequencyEndAt)) {
                    toRepeatEntry.frequencyEndAt = rc.frequencyEndAt;
                }
                toRepeatEntry.createdBy = prc.user;
                toRepeatEntry.modifiedBy = prc.user;
                toRepeatEntry.notes = "Scheduled transfer from #rc.fromAccount.name#";
                toRepeatEntry.save();
                transfer.toRepeatEntry = toRepeatEntry;
            }

            if (rc.deduct gt 0) {
                var deductEntry = entityNew('Entry').new();
                deductEntry.account = rc.toAccount;
                deductEntry.amount = rc.amount;
                deductEntry.occurredAt = rc.frequencyStartAt;
                deductEntry.type = entityNew('EntryType').get(2);
                deductEntry.createdBy = prc.user;
                deductEntry.modifiedBy = prc.user;
                deductEntry.notes = rc.notes;
                deductEntry.save();
                transfer.addEntry(deductEntry);

                if (val(rc.repeat) eq 1) {
                    var deductRepeatEntry = entityNew('RepeatEntry').new();
                    deductRepeatEntry.account = rc.toAccount;
                    deductRepeatEntry.amount = rc.amount;
                    deductRepeatEntry.type = entityNew('EntryType').get(2);
                    deductRepeatEntry.frequency = entityNew('EntryFrequency').get(rc.frequency);
                    deductRepeatEntry.frequencyCount = rc.frequencyCount;
                    deductRepeatEntry.frequencyStartAt = rc.frequencyStartAt;
                    if (isDate(rc.frequencyEndAt)) {
                        deductRepeatEntry.frequencyEndAt = rc.frequencyEndAt;
                    }
                    deductRepeatEntry.createdBy = prc.user;
                    deductRepeatEntry.modifiedBy = prc.user;
                    deductRepeatEntry.notes = "Scheduled after transfer from #rc.fromAccount.name#";
                    deductRepeatEntry.save();
                    transfer.deductRepeatEntry = deductRepeatEntry;
                }
            }

            includeFields.append('fromAccount');
            includeFields.append('toAccount');
            transfer.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: includeFields.toList(),
                composeRelationships: true
            );
            transfer.createdBy = prc.user;
            transfer.modifiedBy = prc.user;
            transfer.save(flush: true);
        }
        setNextEvent('transfers/#transfer.id#');
    }

    void function verifyDelete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.transfer = entityNew('AccountTransfer').get(rc.id);
        if (isNull(prc.transfer.id)) {
            setNextEvent('404');
        }
    }
    
    void function delete(event,rc,prc) {}
}