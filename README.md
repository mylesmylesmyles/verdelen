## MySavingsTracker.ca

A simple little budgeting app in CF, ColdBox and Bootstrap 3.  

## License

Copyright � 2012 Myles Goodhue <myles.goodhue@gmail.com>

Distributed under the Eclipse Public License.