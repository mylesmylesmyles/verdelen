<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.usersList" type="any";
    param name="prc.usersTotal" type="numeric";
    param name="rc.max" default="10";
    param name="rc.offset" default="0";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-10">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Users <span class="badge">#prc.usersTotal#</span></li>
          </ol>
        </div>
        <div class="col-sm-2 pull-right">
          <a href="#event.buildLink('users/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create User</a>
        </div>
      </div>
    </div>
    <table class="table table-hover">
      <thead>
        <tr>
          <th width="25%">Name</th>
          <th width="10%">Is Active</th>
          <th width="10%">Last Login</th>
          <th>Total Entries</th>
          <th>Balance</th>
          <th class="hidden-xs">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        <cfloop query="#prc.usersList#">
        	<tr>
            <td>#esapiEncode('html',prc.usersList.firstName)# #esapiEncode('html',prc.usersList.lastName)#</td>
            <td>#(isBoolean(prc.usersList.isActive) and prc.usersList.isActive ? 'Yes' : 'No')#</td>
            <td>#dateTimeFormat(prc.usersList.lastLogin , 'yyyy-mm-dd HH:nn')#</td>
            <td>#prc.usersList.totalEntries#</td>
            <td>#lsCurrencyFormat(prc.usersList.balance)#</td>
        		<td class="text-center hidden-xs">
              <a href="#event.buildLink('users/#prc.usersList.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
              <a href="#event.buildLink('users/#prc.usersList.id#/delete')#" class="btn btn-danger"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Deactivate</a>
        		</td>
        	</tr>
        </cfloop>
      </tbody>
    </table>
  </div>
</div>
<cfif prc.usersTotal gt 0>
  <cfmodule template="../paging.cfm" 
            offset="#rc.offset#" 
            max="#rc.max#" 
            recordCount="#prc.usersTotal#"
            url="#event.buildLink('users')#"/>
</cfif>
</cfoutput>