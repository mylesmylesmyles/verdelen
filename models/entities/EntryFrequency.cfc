/**
*
* @file  /models/entities/EntryFrequency.cfc
* @author Myles Goodhue
* @description An entry frequency entity
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="EntryFrequency" 
			table="entry_frequencies" {

	this.constraints = {};

	property 	name="id"
				column="id"
				type="numeric"
				sqltype="integer"
				fieldtype="id"
				generator="assigned"
				persistent=true
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="numeric", required=true };

	property 	name="name"
				column="name"
				type="string"
				sqltype="varchar(13)"
				fieldtype="column"
				persistent=true
				length="13"
				required=true
				unique = true 
				uniquekey="uk_entry_frequencies_name"
				notnull=true;

	this.constraints.name = { type="string", required=true, length="1..6" };
}