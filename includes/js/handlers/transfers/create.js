$('#repeat').change(function (e) {
	var checked = $(this).prop('checked');
	(checked) ? $('#repeatOptions').show() : $('#repeatOptions').hide();
	$('#frequency').prop('required', checked);
	$('#frequencyCount').prop('required', checked);
	$('#frequencyStartAt').prop('required', checked);
}).change();
$('#amount').focus();