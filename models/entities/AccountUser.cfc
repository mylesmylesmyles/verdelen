/**
* A cool AccountUser entity
*/
component persistent="true" table="accounts_users" extends="cborm.models.ActiveEntity"{

	// Primary Key
	property name="id" fieldtype="id" column="id" generator="native" setter="false";
	
	// Properties
	property name="account" ormtype="Account";	property name="user" ormtype="User";	property name="permission" ormtype="numeric";	property name="createdAt" ormtype="timestamp";	
	
	// Validation
	this.constraints = {
		// Example: age = { required=true, min="18", type="numeric" }
	};
	
	// Constructor
	function init(){
		super.init( useQueryCaching="false" );
		return this;
	}
}

