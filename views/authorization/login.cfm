<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.xhSubmit" type="string";
    param name="prc.errorMessages" type="array" default=[];
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="well well-lg col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1">
  <h1 class="text-center">Login</h1>

  <cfif prc.errorMessages.len()>
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
      <ul>
        <cfscript>
          #prc.errorMessages.each(function (msg) {
            echo("<li>#msg#</li>")
          })#
        </cfscript>
      </ul>
    </div>
  </cfif>

  <form action="#prc.xhSubmit#" method="post" class="form-horizontal">
    <input type="hidden" name="_securedURL" value="#event.getValue('_securedURL','')#"/>
    <div class="form-group">
      <label for="email" class="control-label sr-only">Enter your login email address</label>
      <div class="input-group input-group-lg">
        <span class="input-group-addon" id="basic-addon1">@</span>
        <input type="email" id="email" name="email" placeholder="Enter your login email address" required class="form-control"/>
      </div>
    </div>
    <div class="form-group">
      <label for="password" class="control-label sr-only">Enter your password</label>
      <div class="input-group input-group-lg">
        <span class="input-group-addon" id="basic-addon1">&bull;&bull;&bull;</span>
        <input type="password" id="password" name="password" placeholder="Enter your password" required class="form-control"/>
      </div>
    </div>
    <div class="form-group">
      <div class="text-center">
        <button type="submit" class="btn btn-success btn-lg col-md-12">Sign in</button>
      </div>
    </div>
  </form>
  </div>
</div>
</cfoutput>