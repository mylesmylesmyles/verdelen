/**
*
* @file  /handlers/Schedule.cfc
* @author Myles Goodhue 
* @description Run scheduled tasks
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        run = "GET"
    };

    void function preHandler(event) {
        
    }

    void function index(event,rc,prc) {
    }

    void function run(event,rc,prc) {
        transaction {
            var entries = entityNew('RepeatEntry')
                .listForDate(forDate: now());
            for (var x = 1; x <= arrayLen(entries); x++) {
                var e = entityNew('Entry').new();
                e.populate(memento: {
                    account: entries[x].account
                    , amount: entries[x].amount
                    , occurredAt: now()
                    , type: entries[x].type
                    , createdAt: now()
                    , createdBy: entries[x].modifiedBy
                    , modifiedAt: now()
                    , modifiedBy: entries[x].modifiedBy
                });
                if (!isNull(entries[x].notes)) {
                    e.notes = entries[x].notes;
                }
                e.save();
            }
            ormFlush();
        }
        event.norender();
    }
}