<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.entry" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center"><a href="#event.buildLink('accounts/#prc.entry.account.id#')#">#esapiEncode('html',prc.entry.account.name)#</a> entry </h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Occurred At</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateFormat(prc.entry.occurredAt, 'yyyy-mm-dd')#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Amount</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #lsCurrencyFormat(prc.entry.amount)#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Balance</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #LSCurrencyFormat(prc.entry.account.getBalance(prc.entry.occurredAt))#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Type</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #prc.entry.type.name#
      </div>
    </div>
    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Notes</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #esapiEncode('html',prc.entry.notes)#
      </div>
    </div>


    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Created</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateFormat(prc.entry.createdAt, 'yyyy-mm-dd')#
      </div>
    </div>

    <div class="row">
      <div class="col-xs-5 col-md-4 text-right">
        <strong>Last modified</strong>
      </div>
      <div class="col-xs-7 col-md-8">
        #dateFormat(prc.entry.modifiedAt, 'yyyy-mm-dd')#
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/entries/#prc.entry.id#/edit')#" class="btn btn-primary">Edit</a>
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/entries/new')#?amount=#esapiEncode('url', prc.entry.amount)#&occurredAt=#esapiEncode('url', dateFormat(prc.entry.occurredAt, 'yyyy-mm-dd'))#&type=#esapiEncode('url', prc.entry.type.id)#&notes=#esapiEncode('url', prc.entry.notes)#" class="btn btn-default">Duplicate</a>
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/entries/#prc.entry.id#/delete')#" class="btn btn-danger">Delete</a>
    </div>
  </div>
</div>
</cfoutput>