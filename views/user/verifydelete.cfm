<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.userAccount" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center">Deactivate <a href="#event.buildLink('users/#prc.userAccount.id#')#">#esapiEncode('html',prc.userAccount.firstName)# #esapiEncode('html',prc.userAccount.lastName)#</a> </h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3">
    <p class="text-danger">Are you certain you want to deactivate this user?</p>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3 text-center">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <form action="#event.buildLink('users/#prc.userAccount.id#/delete')#" method="post" class="form-horizontal">
      <a href="#event.buildLink('users/#prc.userAccount.id#')#" class="btn btn-primary">Cancel</a>
      <input type="submit" value="Deactivate" class="btn btn-danger"/>
      </form>
    </div>
</div>
</cfoutput>