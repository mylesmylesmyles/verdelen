<cfsilent>
  <cfscript>
    param name="prc.pageTitle" default="My Savings Tracker";
    rootDir = getSetting('AppMapping') <= 1 ? "" : "/" & getSetting('AppMapping');
  </cfscript>
</cfsilent>
<cfoutput>#html.doctype()#</cfoutput>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags --->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title><cfoutput>#prc.pageTitle#</cfoutput></title>

    <!--- Bootstrap --->
    <cfoutput>
    <link href="#rootDir#/includes/css/bootstrap.min.css" rel="stylesheet">
    <link href="#rootDir#/includes/css/verdelen.css" rel="stylesheet" />
    </cfoutput>
    <!--- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --->
    <!--- WARNING: Respond.js doesn't work if you view the page via file:// --->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color: #ddd">

    <!-- body content here -->
    <div class="container">
        <cfoutput>#renderView()#</cfoutput>
    </div>

    <cfoutput>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="#rootDir#/includes/js/vendor/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="#rootDir#/includes/js/bootstrap.min.js"></script>
    </cfoutput>
  </body>

</html>
