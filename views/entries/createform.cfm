<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";
    param name="prc.account" type="any";

    param name="prc.xhSubmit" type="string";
    param name="prc.errorMessages" type="array" default=[];
    param name="prc.entryTypeList" type="query";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    <h2 class="text-center">New <a href="#event.buildLink('accounts/#prc.account.id#')#">#esapiEncode('html',prc.account.name)#</a> entry</h2>
    <br>
    <cfif prc.errorMessages.len()>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
        <ul>
          <cfscript>
            #prc.errorMessages.each(function (msg) {
              echo("<li>#msg#</li>")
            })#
          </cfscript>
        </ul>
      </div>
    </cfif>
    <form action="#prc.xhSubmit#" method="post" class="form-horizontal">
      <div class="form-group form-group-lg">
        <label for="amount" class="col-sm-3 control-label">Amount</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
          <cfparam name="rc.amount" default=""/>
          <input type="number" step="0.01" id="amount" name="amount" placeholder="0.00" value="#isNumeric(rc.amount) ? rc.amount : ''#" required class="form-control"/>
        </div>
      </div>
     <div class="form-group form-group-lg">
        <label for="occurredAt" class="col-sm-3 control-label">On date</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
          <cfparam name="rc.occurredAt" default="#dateFormat(now(), 'yyyy-mm-dd')#"/>
          <input type="date" id="occurredAt" name="occurredAt" placeholder="#dateFormat(now(), 'yyyy-mm-dd')#" value="#isDate(rc.occurredAt) ? rc.occurredAt : ''#" required class="form-control">
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="type" class="col-sm-3 control-label">Action</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-piggy-bank"></span></span>
          <cfparam name="rc.type" default=""/>
          <select name="type" id="type" required class="form-control">
            <option value="">Choose...</option>
            <cfloop query="#prc.entryTypeList#">
              <option value="#prc.entryTypeList.id#"<cfif rc.type eq prc.entryTypeList.id> selected</cfif>>#prc.entryTypeList.name#</option>
            </cfloop>
          </select>
        </div>
      </div>
      <div class="form-group form-group-lg">
        <label for="notes" class="col-sm-3 control-label">Notes</label>
        <div class="col-sm-9 input-group input-group-lg">
          <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
          <cfparam name="rc.notes" default=""/>
          <textarea name="notes" rows="5" class="form-control">#esapiEncode('html', rc.notes)#</textarea>
        </div>
      </div>
      <div class="row">
        <div class="text-center">
          <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary btn-lg" value="Create entry"/>
        </div>
      </div>
    </form>
  </div>
</div>
</cfoutput>