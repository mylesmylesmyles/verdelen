<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.budget" type="any";
    param name="prc.accountsList" type="any";
    param name="prc.accountsTotal" type="numeric";
    param name="rc.max" default="10";
    param name="rc.offset" default="0";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
        <div class="col-sm-10">
          <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Accounts <span class="badge">#prc.accountsTotal#</span></li>
          </ol>
        </div>
        <div class="col-sm-2 pull-right">
          <a href="#event.buildLink('accounts/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create Account</a>
        </div>
      </div>
    </div>
    <table class="table table-hover">
      <thead>
        <tr>
          <th width="25%">Name</th>
          <th width="10%">Is Active</th>
          <th width="10%">Last Modified</th>
          <th>Balance</th>
          <th class="hidden-xs">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        <cfloop query="#prc.accountsList#">
        	<tr>
            <td>#esapiEncode('html',prc.accountsList.name)#</td>
            <td>#(isBoolean(prc.accountsList.isActive) and prc.accountsList.isActive ? 'Yes' : 'No')#</td>
            <td>#dateFormat(prc.accountsList.modifiedAt , 'yyyy-mm-dd')#</td>
            <td>#lsCurrencyFormat(prc.accountsList.balance)#</td>
        		<td class="text-center hidden-xs">
              <a href="#event.buildLink('accounts/#prc.accountsList.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
              <a href="#event.buildLink('accounts/#prc.accountsList.id#/entries')#" class="btn btn-info"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Entries</a>
              <a href="#event.buildLink('accounts/#prc.accountsList.id#/entries/new')#" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New entry</a>
        		</td>
        	</tr>
          <tr class="visible-xs">
            <td colspan="5" class="text-center">
              <a href="#event.buildLink('accounts/#prc.accountsList.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"> Details</a>
              <a href="#event.buildLink('accounts/#prc.accountsList.id#/entries')#" class="btn btn-info"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Entries</a>
              <a href="#event.buildLink('accounts/#prc.accountsList.id#/entries/new')#" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> New entry</a>
            </td>
          </tr>
        </cfloop>
      </tbody>
    </table>
  </div>
</div>
<cfif prc.accountsTotal gt 0>
  <cfmodule template="../paging.cfm" 
            offset="#rc.offset#" 
            max="#rc.max#" 
            recordCount="#prc.accountsTotal#"
            url="#event.buildLink('accounts')#"/>
</cfif>
</cfoutput>