﻿component{
	// Application properties
	this.name = hash( getCurrentTemplatePath() );
	this.sessionManagement = true;
	this.sessionTimeout = createTimeSpan(0,0,30,0);
	this.setClientCookies = true;

	this.invokeImplicitAccessor=true;
	this.triggerDataMember = true;

	// COLDBOX STATIC PROPERTY, DO NOT CHANGE UNLESS THIS IS NOT THE ROOT OF YOUR COLDBOX APP
	COLDBOX_APP_ROOT_PATH = getDirectoryFromPath( getCurrentTemplatePath() );
	// The web server mapping to this application. Used for remote purposes or static purposes
	COLDBOX_APP_MAPPING   = cgi.script_name.replace('index.cfm', '');
	// COLDBOX PROPERTIES
	COLDBOX_CONFIG_FILE 	 = "";
	// COLDBOX APPLICATION KEY OVERRIDE
	COLDBOX_APP_KEY 		 = "";

	this.mappings[ "/coldbox" ] = COLDBOX_APP_ROOT_PATH & "coldbox";

	// JAVA INTEGRATION: JUST DROP JARS IN THE LIB FOLDER
	// You can add more paths or change the reload flag as well.
	this.javaSettings = { loadPaths = [ "lib" ], reloadOnChange = false };

	// The app root mapping
	this.mappings[ "/app" ] = COLDBOX_APP_ROOT_PATH;

	// Used by the cborm module
	this.mappings[ "/cborm" ] = COLDBOX_APP_ROOT_PATH & "modules/cborm";

	// ORM settings
	this.mappings[ "/ormmodel" ] = expandPath("models/entities");
	this.ormenabled = true;
	this.datasource = 'verdelen';
	this.ormsettings = {};
	this.ormsettings.datasource = this.datasource;
	this.ormsettings.dialect = "MySQL";
	this.ormsettings.automanageSession = false;
	this.ormsettings.flushatrequestend = false;
	this.ormsettings.cfclocation = ["ormmodel"];
	this.ormSettings.useDBForMapping = false;
	this.ormSettings.autogenmap = true;
	this.ormsettings.eventHandling = true;
	this.ormsettings.logsql = true;
	this.ormsettings.eventhandler = "app.modules.cborm.models.EventHandler";
	if (isDevelopment()) {
		// this.ormsettings.dbcreate = 'dropcreate';
		//this.ormSettings.sqlscript = expandPath("lib/db/import.sql");
	}

	// application start
	boolean function onApplicationStart(){
		application.cbBootstrap = new coldbox.system.Bootstrap( COLDBOX_CONFIG_FILE, COLDBOX_APP_ROOT_PATH, COLDBOX_APP_KEY, COLDBOX_APP_MAPPING );
		application.cbBootstrap.loadColdbox();
		return true;
	}

	// request start
	boolean function onRequestStart(String targetPage){
		if (application.cbBootstrap.isfwReinit()) {
			ormReload();
		}
		// Process ColdBox Request
		application.cbBootstrap.onRequestStart( arguments.targetPage );
		return true;
	}

	void function onSessionStart(){
		application.cbBootStrap.onSessionStart();
	}

	void function onSessionEnd( struct sessionScope, struct appScope ){
		arguments.appScope.cbBootStrap.onSessionEnd( argumentCollection=arguments );
	}

	boolean function onMissingTemplate( template ){
		return application.cbBootstrap.onMissingTemplate( argumentCollection=arguments );
	}

	boolean function isDevelopment() {
		return cgi.http_host eq 'localhost' or cgi.http_host.left(9) eq '127.0.0.1';
	}
}