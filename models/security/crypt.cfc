/**
*
* @file  /models/security/crypt.cfc
* @author Myles Goodhue 
* @description Cryptography library
*
*/

component {

	// PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
	// specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
	variables.algorithm = "PBKDF2WithHmacSHA1";

	// SHA-1 generates 160 bit hashes, so that's what makes sense here
	variables.derivedKeyLength = 160;

	// Pick an iteration count that works for you. The NIST recommends at
	// least 1,000 iterations:
	// http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
	// iOS 4.x reportedly uses 10,000:
	// http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
	variables.iterations = 20000;

	/**
 	 * @hint Constructor
	 */
	crypt function init(){
		return this;
	}

	/**
 	 * @hint Generate a random salt for encrypting strings
 	 * @size The size of the salt
	 */
	binary function genSalt(numeric size=8) {
		// VERY important to use SecureRandom instead of just Random
		var random = createObject("java", "java.security.SecureRandom").getInstance("SHA1PRNG");
		var salt = getByteArray(arguments.size);
		random.nextBytes(salt);
		return salt;
	}

	/**
 	 * @hint Encrypt a string
 	 * @size The size of the salt
	 */
	string function encrypt(required string raw, binary salt=genSalt()) {
		var spec = createObject("java", "javax.crypto.spec.PBEKeySpec").init(
				javaCast("char[]", raw.listToArray(""))
				, arguments.salt
				, javaCast("int", variables.iterations)
				, javaCast("int", variables.derivedKeyLength)
			);
		var f = createObject("java", "javax.crypto.SecretKeyFactory").getInstance(variables.algorithm);
		return f.generateSecret(spec).getEncoded();
	}

	/**
 	 * @hint Base64 encode a byte array
 	 * @size The byte array to encode
	 */
	string function byteToBase64(required binary data) {
		return createObject("java", "sun.misc.BASE64Encoder").init().encode(arguments.data);
	}

	/**
 	 * @hint Turn a Base64 encoded string to a byte array
 	 * @size The string to decode
	 */
	string function base64ToByte(required string data) {
		return createObject("java", "sun.misc.BASE64Decoder").init().decodeBuffer(arguments.data);
	}

	/**
 	 * @hint Create a byte-array of the specified size
 	 * @size The size of the salt
	 */
	public binary function getByteArray(numeric size=8) {
		var emptyByteArray = createObject("java", "java.io.ByteArrayOutputStream").init().toByteArray();
		var byteClass = emptyByteArray.getClass().getComponentType();
		var byteArray = createObject("java","java.lang.reflect.Array").newInstance(byteClass, arguments.size);
		return byteArray;
		return;
	}
}