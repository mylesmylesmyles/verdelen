/**
*
* @file  /models/entities/Account.cfc
* @author Myles Goodhue
* @description A budget account entity
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="Account" 
			table="accounts"
			autowire=true 
			accessors=true {

	property 	name="util"
				persistent=false
				inject="model:util";

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="name"
				column="name"
				type="string"
				sqltype="varchar(30)"
				fieldtype="column"
				persistent=true
				length="30"
				required=true;

	this.constraints.name = { type="string", required=true, length="1..30" };

	property 	name="budget" 
				fieldtype="many-to-one" 
				cfc="Budget" 
				fkcolumn="budgets_id" 
				column="id" 
				cascade="delete" 
				lazy=true ;

	this.constraints.budget = { required=true };

	property 	name="isActive"
				column="is_active"
				type="boolean"
				sqltype="boolean"
				fieldtype="column"
				persistent=true
				required=true
				dbdefault=true;

	this.constraints.isActive = { type="boolean", required=true };

	property 	name="modifiedAt"
				column="modified_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated="always"
				persistent=true
				required=true
				notnull=true;

	this.constraints.modifiedAt = { type="date", required=true };

	void function preInsert() {
		variables.modifiedAt = now();
	}

	void function preUpdate() {
		variables.modifiedAt = now();
	}

	/**
 	 * @hint Return the account balance
	 */
	any function getBalance(date onDate=now()) {
		var connection = ormGetSession().connection();
		var stmt = connection.prepareStatement("
			select sum(case when entry_types_id = 1 then amount else 0 end) - 
				sum(case when entry_types_id = 2 then amount else 0 end) as balance
			from entries
			where accounts_id = ?
			and date_trunc('day', occurred_at) <= cast('" & dateFormat(arguments.onDate, 'yyyy-mm-dd') & "' as timestamp)
		");
		stmt.setString(1, variables.id);
		stmt.executeQuery();
		var rs = stmt.getResultSet();
		rs.next();
		return rs.getFloat(1);
	}

	/**
 	 * @override The postgres db doesn't support the current syntax used by the orm :(
	 */
	query function list(
			struct criteria=structnew(),
			string sortOrder="",
			numeric offset=0,
			numeric max=0,
			numeric timeout=0,
			boolean ignoreCase=false
		) {
		var qry = new Query();
		var sql = "
			with data as (
				select accounts.id,
					accounts.is_active as isactive,
					accounts.name,
					accounts.budgets_id,
					accounts.modified_at as modifiedAt,
					sum(case when entries.entry_types_id = 1 then entries.amount else 0 end) - 
						sum(case when entries.entry_types_id = 2 then entries.amount else 0 end) as balance
				from accounts
				left join entries
					on entries.accounts_id = accounts.id
		";
		if (arguments.criteria.keyExists('owner')) {
			sql &= " join budgets on budgets.id = accounts.budgets_id 
				and budgets.owner_users_id = :owner ";
			qry.addParam(name: 'owner', value: arguments.criteria.owner.id, cfsqltype: 'cf_sql_varchar');
		}
		sql &=" 
				group by accounts.id,
					accounts.is_active,
					accounts.name,
					accounts.budgets_id,
					accounts.modified_at
				order by name asc
			), counter as (
				select count(data.id) as total from data
			) 
			select data.*, counter.total 
			from data, counter 
		";
		if (arguments.max > 0) {
			sql &= " limit :max ";
			qry.addParam(name: "max", value=arguments.max, cfsqltype: 'cf_sql_integer');
		} 
		if (arguments.offset > 0) {
			sql &= " offset :offset ";
			qry.addParam(name: "offset", value=arguments.offset, cfsqltype: 'cf_sql_integer');
		} 
		return qry.setSQL(sql).execute().getResult();
		
	}

	/**
 	 * @hint Total entries
	 */
	any function entryCount() {
		return new Query()
			.setSQl("
				select count(id) as total
				from entries
				where accounts_id = :id
				and date_trunc('day', occurred_at) <= date_trunc('day', current_timestamp)
			")
			.addParam(name: 'id', value: variables.id)
			.execute()
			.getResult()
			.total;
	}

	/**
 	 * @hint Total contributions and deductions by month
	 */
	any function totalByMonth() {
		return new Query()
			.setSQl("
			select sum(case when entry_types_id = 1 then amount else 0 end) as contributions, 
				sum(case when entry_types_id = 2 then amount else 0 end) as deductions,
				date_trunc('month', occurred_at) forMonth
			from entries
			where accounts_id = :id
			and date_trunc('day', occurred_at) <= date_trunc('day', current_timestamp)
			and date_trunc('month', occurred_at) >= date_trunc('month', current_timestamp - interval '12 months')
			group by date_trunc('month', occurred_at)
			order by forMonth desc
		")
		.addParam(name: 'id', value: variables.id)
		.execute()
		.getResult();
	}
}