<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

  param name="prc.account" type="any";
  param name="prc.entryList" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
<div class="panel panel-default">
  <div class="panel-heading">#esapiEncode('html',prc.account.name)# &raquo; Repeat Entries
<a href="#event.buildLink('accounts/#prc.account.id#/repeatentries/new')#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div>
  <table class="table table-hover">
    <thead>
    	<tr>
        <th colspan="4">&nbsp;</th>
        <th colspan="4" class="text-center">Frequency</th>
        <th colspan="2" class="hidden-xs">Created</th>
        <th colspan="2" class="hidden-xs">Modified</th>
      </tr>
  		<tr>
  			<th>&nbsp;</td>
        <th>Amount</th>
        <th>Type</th>
        <th>Frequency</th>
        <th>Count</th>
        <th>Start</th>
        <th>End</th>
        <th class="hidden-xs">On</th>
        <th class="hidden-xs">By</th>
        <th class="hidden-xs">On</th>
        <th class="hidden-xs">By</th>
  		</tr>
    </thead>
    <tbody>
      <cfloop array="#prc.entryList#" item="entry">
      	<tr>
      		<td>
      			<a href="#event.buildLink('accounts/#prc.account.id#/repeatentries/#entry.id#')#" class="btn btn-primary"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span></a>
        		<a href="#event.buildLink('accounts/#prc.account.id#/repeatentries/#entry.id#/delete')#" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
          </td>
          <td>#LSCurrencyFormat(entry.amount)#</td>
          <td>#entry.type.name#</td>
          <td>#isNull(entry.frequency.name) ? '' : entry.frequency.name#</td>
          <td>#isNull(entry.frequencyCount) ? '' : entry.frequencyCount#</td>
          <td>#isNull(entry.frequencyStartAt) ? '' : dateFormat(entry.frequencyStartAt, 'yyyy-mm-dd')#</td>
          <td>#isNull(entry.frequencyEndAt) ? '' : dateFormat(entry.frequencyEndAt, 'yyyy-mm-dd')#</td>
          <td class="hidden-xs">#dateFormat(entry.createdAt, 'yyyy-mm-dd')#</td>
          <td class="hidden-xs">#esapiEncode('html', entry.createdBy.firstName)# #esapiEncode('html', entry.createdBy.lastName)#</td>
          <td class="hidden-xs">#dateFormat(entry.modifiedAt, 'yyyy-mm-dd')#</td>
          <td class="hidden-xs">#esapiEncode('html', entry.modifiedBy.firstName)# #esapiEncode('html', entry.modifiedBy.lastName)#</td>
      	</tr>
      </cfloop>
    </tbody>
  </table>
  </div>
</div>
</cfoutput>