drop table if exists accounts_transfers;
drop table if exists repeat_entries;
drop table if exists entries;
drop table if exists accounts;
drop table if exists budgets;
drop table if exists users;
drop table if exists entry_frequencies;
drop table if exists entry_types;

CREATE TABLE public.entry_types
(
   id integer NOT NULL, 
   name character varying(40) NOT NULL, 
   CONSTRAINT entry_types_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entry_types
  OWNER TO verdelen;
COMMENT ON COLUMN public.entry_types.id IS 'The primary key';
COMMENT ON COLUMN public.entry_types.name IS 'The type name';
COMMENT ON TABLE public.entry_types
  IS 'Types of account entries';

CREATE TABLE public.entry_frequencies
(
   id integer NOT NULL, 
   name character varying(13) NOT NULL, 
   CONSTRAINT entry_frequencies_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entry_frequencies
  OWNER TO verdelen;
COMMENT ON COLUMN public.entry_frequencies.id IS 'The primary key';
COMMENT ON COLUMN public.entry_frequencies.name IS 'The frequency name';
COMMENT ON TABLE public.entry_frequencies
  IS 'The account entry freqencies';


CREATE TABLE public.users
(
   id character varying(36) NOT NULL, 
   last_name character varying(30) NOT NULL, 
   first_name character varying(30) NOT NULL, 
   email character varying(320) NOT NULL, 
   admin boolean NOT NULL DEFAULT FALSE, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   last_login timestamp without time zone, 
   pass character varying(100), 
   salt character varying(100), 
   CONSTRAINT users_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.users
  OWNER TO verdelen;
COMMENT ON COLUMN public.users.first_name IS 'The account first name';
COMMENT ON COLUMN public.users.last_name IS 'The account surname';
COMMENT ON COLUMN public.users.email IS 'The email address';
COMMENT ON COLUMN public.users.admin IS 'Is the user an administrator?';
COMMENT ON COLUMN public.users.is_active IS 'Is the user account active?';
COMMENT ON COLUMN public.users.last_login IS 'The date time the last time the account logged in';
COMMENT ON COLUMN public.users.pass IS 'The hashed account password';
COMMENT ON COLUMN public.users.salt IS 'The password encryption salt';
COMMENT ON TABLE public.users
  IS 'Application user accounts';


CREATE TABLE public.budgets
(
   id character varying(36) NOT NULL, 
   name character varying(30) NOT NULL, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   owner_users_id character varying(36) NOT NULL, 
   CONSTRAINT budgets_pk PRIMARY KEY (id), 
   CONSTRAINT budgets_owners_fk FOREIGN KEY (owner_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.budgets
  OWNER TO verdelen;
COMMENT ON COLUMN public.budgets.id IS 'The unique budget id';
COMMENT ON COLUMN public.budgets.name IS 'The budget name';
COMMENT ON COLUMN public.budgets.is_active IS 'Is the buget currently in use?';
COMMENT ON COLUMN public.budgets.created_at IS 'The date/time the budget was created';
COMMENT ON COLUMN public.budgets.owner_users_id IS 'FK to USERS.ID
The user who owns the budget';
COMMENT ON TABLE public.budgets
  IS 'User budgets';

CREATE TABLE public.accounts
(
   id character varying(36) NOT NULL, 
   name character varying(30) NOT NULL, 
   budgets_id character varying(36) NOT NULL, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   CONSTRAINT accounts_pk PRIMARY KEY (id), 
   CONSTRAINT accounts_budgets_fk FOREIGN KEY (budgets_id) REFERENCES budgets (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
COMMENT ON COLUMN public.accounts.id IS 'The primary key';
COMMENT ON COLUMN public.accounts.name IS 'The account name';
COMMENT ON COLUMN public.accounts.budgets_id IS 'FK to BUDGETS.ID';
COMMENT ON COLUMN public.accounts.is_active IS 'Is the account currently active?';
COMMENT ON COLUMN public.accounts.modified_at IS 'The last time the account was modified';
COMMENT ON TABLE public.accounts
  IS 'Budget accounts';

CREATE TABLE public.entries
(
   id character varying(36) NOT NULL, 
   amount double precision NOT NULL DEFAULT 0, 
   accounts_id character varying(36) NOT NULL, 
   occurred_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   entry_types_id integer NOT NULL, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT entries_pk PRIMARY KEY (id), 
   CONSTRAINT entries_modified_users_id_fk FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE RESTRICT, 
   CONSTRAINT entries_created_users_id_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE RESTRICT, 
   CONSTRAINT entries_types_id_fk FOREIGN KEY (entry_types_id) REFERENCES entry_types (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entries
  OWNER TO verdelen;
COMMENT ON COLUMN public.entries.id IS 'The primary key';
COMMENT ON COLUMN public.entries.amount IS 'The entry amount';
COMMENT ON COLUMN public.entries.accounts_id IS 'FK to ACCOUNTS.ID';
COMMENT ON COLUMN public.entries.occurred_at IS 'The date/time the entry occurred';
COMMENT ON COLUMN public.entries.entry_types_id IS 'FK to ENTRY_TYPES.ID';
COMMENT ON COLUMN public.entries.notes IS 'Any notes regarding the entry';
COMMENT ON COLUMN public.entries.created_at IS 'The date/time the entry was created';
COMMENT ON COLUMN public.entries.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.entries.modified_at IS 'The date/time the entry was last modified';
COMMENT ON COLUMN public.entries.modified_users_id IS 'FK to USERS.ID';
COMMENT ON TABLE public.entries
  IS 'Account entries';

CREATE TABLE public.accounts_transfers
(
   id character varying(36) NOT NULL, 
   amount double precision NOT NULL, 
   from_accounts_id character varying(36) NOT NULL, 
   to_accounts_id character varying(36) NOT NULL, 
   entry_frequency_id integer, 
   entry_frequencies_interval integer DEFAULT 0, 
   entry_frequencies_count integer DEFAULT 1, 
   entry_frequencies_start_at timestamp without time zone, 
   entry_frequencies_end_at timestamp without time zone, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT account_transfers_pk PRIMARY KEY (id), 
   CONSTRAINT account_transfers_from_accounts_fk FOREIGN KEY (from_accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_to_accounts_fk FOREIGN KEY (to_accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_frequencies_fk FOREIGN KEY (entry_frequency_id) REFERENCES entry_frequencies (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_created_users_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT account_transfers_modified_users_id FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.accounts_transfers
  OWNER TO verdelen;
COMMENT ON COLUMN public.accounts_transfers.id IS 'The primary key';
COMMENT ON COLUMN public.accounts_transfers.amount IS 'The amount being transfered';
COMMENT ON COLUMN public.accounts_transfers.from_accounts_id IS 'Transfering from this account';
COMMENT ON COLUMN public.accounts_transfers.to_accounts_id IS 'Transfering to this account';
COMMENT ON COLUMN public.accounts_transfers.entry_frequency_id IS 'The frequency of the transfer';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_interval IS 'The entry frequency interval';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_count IS 'The number of entires';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_start_at IS 'The start date/time when the transfers will begin';
COMMENT ON COLUMN public.accounts_transfers.entry_frequencies_end_at IS 'The end date/time when the last transfer will occur';
COMMENT ON COLUMN public.accounts_transfers.notes IS 'Notes about the transfer';
COMMENT ON COLUMN public.accounts_transfers.created_at IS 'The date/time the record was created';
COMMENT ON COLUMN public.accounts_transfers.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.accounts_transfers.modified_at IS 'The date/time the record was last modified';
COMMENT ON COLUMN public.accounts_transfers.modified_users_id IS 'FK to USERS.ID';
COMMENT ON TABLE public.accounts_transfers
  IS 'Transfers from one account to another';

CREATE TABLE public.repeat_entries
(
   id character varying(36) NOT NULL, 
   accounts_id character varying(36) NOT NULL, 
   amount double precision NOT NULL, 
   entry_types_id integer NOT NULL, 
   entry_frequency_id integer NOT NULL, 
   entry_frequencies_count integer NOT NULL DEFAULT 1, 
   entry_frequencies_interval integer NOT NULL DEFAULT 0, 
   entry_frequencies_start_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   entry_frequencies_end_at timestamp without time zone, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT repeat_entries_pk PRIMARY KEY (id), 
   CONSTRAINT repeat_entreies_accounts_fk FOREIGN KEY (accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_types_fk FOREIGN KEY (entry_types_id) REFERENCES entry_types (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_frequency_fk FOREIGN KEY (entry_frequency_id) REFERENCES entry_frequencies (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_created_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_modified_fk FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.repeat_entries
  OWNER TO verdelen;
COMMENT ON COLUMN public.repeat_entries.id IS 'The primary key';
COMMENT ON COLUMN public.repeat_entries.accounts_id IS 'The account the entry will be placed in';
COMMENT ON COLUMN public.repeat_entries.amount IS 'The entry amount';
COMMENT ON COLUMN public.repeat_entries.entry_types_id IS 'The entry type';
COMMENT ON COLUMN public.repeat_entries.entry_frequency_id IS 'The frequency of the entry';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_count IS 'The number of the entry frequencies';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_interval IS 'The entry frequency interval';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_start_at IS 'The date/time when entries will begin';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_end_at IS 'The date/time when the last entry will be created';
COMMENT ON COLUMN public.repeat_entries.notes IS 'Notes about the entry';
COMMENT ON COLUMN public.repeat_entries.created_at IS 'The date/time when record was created';
COMMENT ON COLUMN public.repeat_entries.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.repeat_entries.modified_at IS 'The date/time the record was last modified';
COMMENT ON COLUMN public.repeat_entries.modified_users_id IS 'The user who last modified the record';
COMMENT ON TABLE public.repeat_entries
  IS 'Repeating account entries';
