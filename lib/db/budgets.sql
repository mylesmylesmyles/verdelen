CREATE TABLE public.budgets
(
   id character varying(36) NOT NULL, 
   name character varying(30) NOT NULL, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   owner_users_id character varying(36) NOT NULL, 
   CONSTRAINT budgets_pk PRIMARY KEY (id), 
   CONSTRAINT budgets_owners_fk FOREIGN KEY (owner_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.budgets
  OWNER TO verdelen;
COMMENT ON COLUMN public.budgets.id IS 'The unique budget id';
COMMENT ON COLUMN public.budgets.name IS 'The budget name';
COMMENT ON COLUMN public.budgets.is_active IS 'Is the buget currently in use?';
COMMENT ON COLUMN public.budgets.created_at IS 'The date/time the budget was created';
COMMENT ON COLUMN public.budgets.owner_users_id IS 'FK to USERS.ID
The user who owns the budget';
COMMENT ON TABLE public.budgets
  IS 'User budgets';
