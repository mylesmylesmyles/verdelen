CREATE TABLE public.entry_frequencies
(
   id integer NOT NULL, 
   name character varying(13) NOT NULL, 
   CONSTRAINT entry_frequencies_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entry_frequencies
  OWNER TO verdelen;
COMMENT ON COLUMN public.entry_frequencies.id IS 'The primary key';
COMMENT ON COLUMN public.entry_frequencies.name IS 'The frequency name';
COMMENT ON TABLE public.entry_frequencies
  IS 'The account entry freqencies';
