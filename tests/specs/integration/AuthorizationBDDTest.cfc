/*******************************************************************************
*	Integration Test as BDD
*
*	Extends the integration class: coldbox.system.testing.BaseTestCase
*
*	so you can test your ColdBox application headlessly. The 'appMapping' points by default to
*	the '/root' mapping created in the test folder Application.cfc.  Please note that this
*	Application.cfc must mimic the real one in your root, including ORM settings if needed.
*
*	The 'execute()' method is used to execute a ColdBox event, with the following arguments
*	* event : the name of the event
*	* private : if the event is private or not
*	* prePostExempt : if the event needs to be exempt of pre post interceptors
*	* eventArguments : The struct of args to pass to the event
*	* renderResults : Render back the results of the event
*******************************************************************************/
component extends="coldbox.system.testing.BaseTestCase" appMapping="/root"{

	/*********************************** LIFE CYCLE Methods ***********************************/

	function beforeAll(){
		super.beforeAll();
		// do your own stuff here
	}

	function afterAll(){
		// do your own stuff here
		super.afterAll();
	}

/*********************************** BDD SUITES ***********************************/

	function run(){

		describe( "Authorization Handler", function(){

			beforeEach(function( currentSpec ){
				// Setup as a new ColdBox request, VERY IMPORTANT. ELSE EVERYTHING LOOKS LIKE THE SAME REQUEST.
				setup();
			});

			afterEach(function( currentSpec ){
				getModel('sessionStorage@cbstorages').deleteVar('user');
			});

			it( "+login page renders", function(){
				var event = execute( event="authorization.loginPage", renderResults=true );
				expect(	event.getValue( name="xhSubmit", private=true ) ).toBe( event.buildLink('/login') );
			});

			xdescribe( "Login validation", function(){
				beforeEach(function( currentSpec ){
					// Setup as a new ColdBox request, VERY IMPORTANT. ELSE EVERYTHING LOOKS LIKE THE SAME REQUEST.
					setup();
				});

				it( "+validate credentials renders", function(){
					//You need to create an exception bean first and place it on the request context FIRST as a setup.
					var requestBean = getMockRequestContext();

					// getRequestContext().setHttpRequestMethod("POST");
					var event = execute( event="authorization.validateCredentials", renderResults=true );
				});
			});


			it( "+login success page renders", function(){
				getModel('sessionStorage@cbstorages').setVar('user', '1');
				var event = execute( event="authorization.loginSuccess", renderResults=true );
			});

			it( "+logout page renders", function(){
				var event = execute( event="authorization.logout", renderResults=true );
			});


		});

	}

}