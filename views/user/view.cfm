<cfoutput> 
  <div class="panel panel-info col-md-12">
    <h3 class="panel-heading">User</h3>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Name:</strong></div>
        <div class="col-md-8 col-sm-12">#esapiEncode('html',prc.userAccount.firstName)# #esapiEncode('html',prc.userAccount.lastName)#</div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Email:</strong></div>
        <div class="col-md-8 col-sm-12">#esapiEncode('html',prc.userAccount.email)# </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Last login:</strong></div>
        <div class="col-md-8 col-sm-12">#dateFormat(prc.userAccount.lastLogin, 'yyyy-mm-dd')# #timeFormat(prc.userAccount.lastLogin, 'HH:mm:ss')#</div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Active:</strong></div>
        <div class="col-md-8 col-sm-12">#prc.userAccount.isActive ? 'True' : 'False'#</div>
      </div>
    </div>
  </div>
  
  <cfif prc.user.admin>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
          <a href="#event.buildLink('users/#prc.userAccount.id#/edit')#" class="btn btn-primary">Edit</a>
          <a href="#event.buildLink('users/#prc.userAccount.id#/delete')#" class="btn btn-danger">Deactivate</a>
        </div>
      </div>
    </div>
  </cfif>
</cfoutput>