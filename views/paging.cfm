<cfscript>
param name="attributes.url";
param name="attributes.offset" type="numeric";
param name="attributes.max" type="numeric";
param name="attributes.recordCount" type="numeric";

maxpages = 10;
totalPages = int(attributes.recordCount/attributes.max);
if (attributes.recordCount mod attributes.max gt 0) {
	totalPages++;
}
limit = totalPages;
start = 1;
if (limit > maxpages) {
	current = attributes.offset/attributes.max;
	if ((current+5) > limit) {
		start = limit - maxpages > 0 ? limit - maxpages : 1;
	} else if (current > 5) {
		start = current - 5;
	}
	limit = start + maxpages;
}
</cfscript>
<cfoutput>
<div class="row">
<nav class="text-center">
  <ul class="pagination pagination-lg">
    <li class="#val(attributes.offset) eq 0 ? 'disabled' : ''#">
      <a href="#attributes.url#?max=#attributes.max#&offset=#attributes.offset-attributes.max#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <cfif start neq 1 and val(attributes.offset) neq 0>
	    <li>
	      <a href="#attributes.url#?max=#attributes.max#&offset=0" aria-label="First page">
	        <span aria-hidden="true">1</span>
	      </a>
	    </li>
	    <li><span aria-hidden="true">...</span></li>
    </cfif> 
    <cfloop from="#start#" to="#limit#" index="x">
      <li class="#val(attributes.offset) eq ((x-1)*attributes.max) ? 'active' : ''#">
        <a href="#attributes.url#?max=#attributes.max#&offset=#(x-1)*attributes.max#">#x#</a>
      </li>  
    </cfloop>
    <cfif limit < totalPages>
	    <li><span aria-hidden="true">...</span></li>
	    <li>
	      <a href="#attributes.url#?max=#attributes.max#&offset=#totalPages*attributes.max#" aria-label="Last page">
	        <span aria-hidden="true">#totalPages#</span>
	      </a>
	    </li>
    </cfif>
    <li class="#attributes.recordCount - attributes.offset < attributes.max ? 'disabled' : ''#">
      <a href="#attributes.url#?max=#attributes.max#&offset=#attributes.offset+attributes.max#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
</div>
</cfoutput>
<cfexit method="exittag"/>