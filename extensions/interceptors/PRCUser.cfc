/**
*
* @file  /extensions/interceptors/PRCUser.cfc
* @author Myles Goodhue  
* @description Add the user object to the PRC scope
*
*/

component extends="coldbox.system.Interceptor" {
	boolean function preProcess(event,interceptData,buffer) {
    	var prc = event.getCollection(private: true);
    	var uid = getInstance('sessionStorage@cbstorages').getVar('user');
    	if (isNull(uid) or uid.len() eq 0) {
    		prc.user = entityNew('User').new();
    	} else {
    		prc.user = entityNew('User').get(uid);
    	}

        // Add the user to the RC scope so that when events are cached 
        // they are unique to the current user
        var rc = event.getCollection();
        rc.userid = uid;

		return false;
	}
}