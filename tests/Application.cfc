﻿/**
********************************************************************************
Copyright 2005-2007 ColdBox Framework by Luis Majano and Ortus Solutions, Corp
www.ortussolutions.com
********************************************************************************
*/
component{

	// APPLICATION CFC PROPERTIES
	this.name 				= "ColdBoxTestingSuite" & hash(getCurrentTemplatePath());
	this.sessionManagement 	= true;
	this.sessionTimeout 	= createTimeSpan( 0, 0, 15, 0 );
	this.applicationTimeout = createTimeSpan( 0, 0, 15, 0 );
	this.setClientCookies 	= true;

	// Create testing mapping
	this.mappings[ "/tests" ] = getDirectoryFromPath( getCurrentTemplatePath() );
	// Map back to its root
	rootPath = REReplaceNoCase( this.mappings[ "/tests" ], "tests(\\|/)", "" );
	this.mappings["/root"]   = rootPath;

	COLDBOX_APP_ROOT_PATH = getDirectoryFromPath( getCurrentTemplatePath() ) & '../';

	// The app root mapping
	this.mappings[ "/app" ] = COLDBOX_APP_ROOT_PATH;

	this.mappings[ "/coldbox" ] = COLDBOX_APP_ROOT_PATH & "coldbox";

	// Used by the cborm module
	this.mappings[ "/cborm" ] = COLDBOX_APP_ROOT_PATH & "modules/cborm";

	this.mappings[ "/ormmodel" ] = COLDBOX_APP_ROOT_PATH & "models/entities";
	
}