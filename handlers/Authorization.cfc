/**
*
* @file  /handlers/Authorization.cfc
* @author  
* @description
*
*/

component extends="coldbox.system.EventHandler"{

	// Allowed http methods
	this.allowedMethods = { 
        login = "GET",
        validateCredentials = "POST",
        logout = "GET,POST"
    };
	
	/**
	 * @hint Display the login page
	 */
	void function login(event,rc,prc){
		// Submit exit handler
		prc.xhSubmit = event.buildLink('login');

		// An array of error messages
		event.paramValue( name='errorMessages', value=[], private=true );

		event.setView(view="authorization/login", layout="simple");
	}
	
	/**
	 * @hint Validate the client credentials
	 */
	void function validateCredentials(event,rc,prc) {
		event.paramValue( name='email', value='' );
		event.paramValue( name='password', value='' );
		event.paramValue( name='_securedURL', value='' );

		// Keep track of error messages
		var errorMessages = [];

		// Input validation
		if (!rc.password.trim().len()) {
			errorMessages.append("You must enter a password.");
		}

		if (!rc.email.trim().len()) {
			errorMessages.append("You must enter a login email.");
		} else {
			var user = entityNew("User").findByEmail(rc.email);
			if (isNull(user)) {
				errorMessages.append("The credentials you have entered are invalid.");
			} else if (rc.password.trim().len() and !user.isValidPassword(rc.password.trim())) {
				errorMessages.append("The credentials you have entered are invalid.");
			} else {
				getInstance('sessionStorage@cbstorages').setVar('user', user.getID());
			}
		}

		if (errorMessages.len()) {
			flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
			setNextEvent("/login");
		}

        transaction {
            entityMerge(user);
            user.lastLogin = now();
            user.save();
        }
        
		if (rc._securedURL.len()) {
			setNextEvent(url: rc._securedURL);
			return;
		}

		setNextEvent("/");
	}

	void function logout(event,rc,prc) {
		getInstance('sessionStorage@cbstorages').deleteVar('user');
		event.setView(view="authorization/logout", layout="simple");
	}
}