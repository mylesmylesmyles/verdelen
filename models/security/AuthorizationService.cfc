/**
*
* @file  /models/security/AuthorizationService.cfc
* @author  Myles Goodhue 
* @description The authorization service
*
*/

component {

	AuthorizationService function init(){
		return this;
	}

	boolean function userValidator(required struct rule, required coldbox.system.web.Controller controller) {
		var user = controller.getWireBox().getInstance('sessionStorage@cbstorages').getVar('user');
		if (isNull(user) or user.len() eq 0) {
			return false;
		}
		user = entityNew('User').get(user);
		return !isNull(user) and isObject(user) and user.getIsActive();
	}
}