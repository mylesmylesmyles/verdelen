<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.account" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
  <h1 class="text-center">#esapiEncode('html',prc.account.name)# </h1>
    <div class="row">
      <div class="col-md-5 col-md-offset-1">
        <h3><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> Balance
        <small>#LSCurrencyFormat(prc.account.getBalance())#</small></h3>
      </div>

      <div class="col-md-5 col-md-offset-1">
        <h3><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Total entries
        <small>#prc.account.entryCount()#</small></h3>
      </div>
    </div>
</div>
<div class="row">
  <div class="col-sm-6 text-center">
    <a href="#event.buildLink("accounts/#prc.account.id#/entries/new")#" class="btn btn-success btn-block btn-lg" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create entry</a>
  </div>
  <div class="col-sm-6 text-center">
    <a href="#event.buildLink('accounts/#prc.account.id#/entries/')#" class="btn btn-primary btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Entries</a>
  </div>
</div>

<div class="row hidden-xs">
  <div class="clearfix"><br/></div>
</div>

<div class="row">
  <div class="col-sm-6 text-center">
    <a href="#event.buildLink("accounts/#prc.account.id#/repeatentries/new")#" class="btn btn-info btn-block btn-lg" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Create repeat entry</a>
  </div>
  <div class="col-sm-6 text-center">
    <a href="#event.buildLink('accounts/#prc.account.id#/repeatentries/')#" class="btn btn-primary btn-block btn-lg" role="button"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Repeat Entries</a>
  </div>
</div>

<div class="row">
  <div class="panel-body">
    <table class="table table-hover">
        <thead>
          <tr>
            <th>Month</td>
            <th>Contributions</th>
            <th>Deductions</th>
            <th>Balance</th>
        </tr>
        </thead>
        <tbody>
          <cfset qry = prc.account.totalByMonth()/>
          <cfset total = {contributions = 0, deductions = 0}/>
          <cfloop query="#qry#">
            <cfset total.contributions += qry.contributions/>
            <cfset total.deductions += qry.deductions/> 
            <tr>
              <td>#lsDateFormat(qry.formonth, 'mmmm yyyy')#</td>
              <td>#lsCurrencyFormat(qry.contributions)#</td>
              <td>#lsCurrencyFormat(qry.deductions)#</td>
              <td>#lsCurrencyFormat(qry.contributions - qry.deductions)#</td>
            </tr>
          </cfloop>
          <tr>
            <th>Average</th>
            <th>#lsCurrencyFormat(qry.recordCount gt 0 ? total.contributions/qry.recordCount : 0)#</th>
            <th>#lsCurrencyFormat(qry.recordCount gt 0 ? total.deductions/qry.recordCount : 0)#</th>
            <th>#lsCurrencyFormat(qry.recordCount gt 0 ? (total.contributions - total.deductions)/qry.recordCount : 0)#</th>
        </tbody>
    </table>
  </div>
</div>
</cfoutput>