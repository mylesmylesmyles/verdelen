/**
*
* @file  /models/entities/User.cfc
* @author Myles Goodhue
* @description A user entity
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity" 
			persistent=true 
			entityname="User" 
			table="users" 
			autowire=true 
			accessors=true {

	property 	name="crypt"
				persistent=false
				inject="model:security.crypt";

	property 	name="util"
				persistent=false
				inject="model:util";

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="firstName"
				column="first_name"
				type="string"
				sqltype="varchar(30)"
				fieldtype="column"
				persistent=true
				length="30"
				required=true
				notnull=true;

	this.constraints.firstName = { type="string", required=true, length="2..30" };

	property 	name="lastName"
				column="last_name"
				type="string"
				sqltype="varchar(30)"
				fieldtype="column"
				persistent=true
				length="30"
				required=true
				notnull=true;

	this.constraints.lastName = { type="string", required=true, length="2..30" };

	property 	name="email"
				column="email"
				type="string"
				sqltype="varchar(320)"
				fieldtype="column"
				persistent=true
				length="320"
				required=true
				notnull=true
				unique = true 
				uniquekey="uk_user_email";

	this.constraints.email = { type="string", required=true, length="2..320" };

	property 	name="admin"
				column="admin"
				type="boolean"
				sqltype="boolean"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true
				dbdefault=false;

	this.constraints.admin = { type="boolean", required=true };

	property 	name="isActive"
				column="is_active"
				type="boolean"
				sqltype="boolean"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true
				dbdefault=true;

	this.constraints.isActive = { type="boolean", required=true };

	property 	name="lastLogin"
				column="last_login"
				type="timestamp"
				sqltype="timestamp"
				fieldtype="timestamp"
				source="db"
				generated=false
				persistent=true
				required=false;

	this.constraints.lastLogin = { type="date", required=false };

	property 	name="password"
				column="pass"
				type="string"
				sqltype="varchar(100)"
				fieldtype="column"
				persistent=true
				length="100"
				required=false;

	this.constraints.password = { type="string", required=false, length="1..100" };

	property 	name="salt"
				column="salt"
				type="string"
				sqltype="varchar(100)"
				fieldtype="column"
				persistent=true
				length="100"
				required=true
				notnull=true;

	this.constraints.salt = { type="string", required=false, length="1..100" };


	property 	name="isPersisted"
				type="boolean"
				persistent=false;


	User function init() {
		super.init(argumentCollection: arguments);
		variables.isPersisted = false;
		return this;
	}
	
	void function postLoad() {
		variables.isPersisted = true;
	}

	/**
 	 * @hint Set the password and encrypt it
 	 * @password.hint The new password
	 */
	any function setPassword(required string password) {
		if (isNull(variables.salt)) {
			variables.salt =  getCrypt().byteToBase64(getCrypt().genSalt());
		}
		variables.password = encryptPassword(arguments.password);
		return this;
	}

	/**
 	 * @hint Test if the new password is the same as the password for the user
 	 * @password.hint The password to test
	 */
	boolean function isValidPassword(required string password) {
		return !compare(getPassword(), encryptPassword(arguments.password));
	}

	/**
 	 * @hint Encrypt a password
 	 * @password.hint The password
	 */
	private string function encryptPassword(required string password) {
		return getCrypt().byteToBase64(
			getCrypt().encrypt(arguments.password,  getCrypt().base64ToByte(getSalt()))
		);
	}

	/**
	 * @hint Return the recent activity for a user
	 */
	query function listRecentActivity(numeric offset=0, numeric max=0) {
		return;
	}
	
	/**
 	 * @override The postgres db doesn't support the current syntax used by the orm :(
	 */
	query function list(
			struct criteria=structnew(),
			string sortOrder="",
			numeric offset=0,
			numeric max=0,
			numeric timeout=0,
			boolean ignoreCase=false
		) {
		var qry = new Query();
		var sql = "
			with data as (
				select users.id,
					users.is_active as isactive,
					users.first_name firstName,
					users.last_name lastName,
					users.email,
					users.admin,
					users.last_login lastLogin,
					users.is_Active isActive,
					count(budgets.id) as totalBudgets,
					count(entries.id) as totalEntries,
					sum(case when entries.entry_types_id = 1 then entries.amount else 0 end) - 
						sum(case when entries.entry_types_id = 2 then entries.amount else 0 end) as balance
				
				from users
				left join budgets
					on budgets.owner_users_id = users.id
				left join entries
					on entries.created_users_id = users.id

				group by users.id,
					users.is_active,
					users.first_name,
					users.last_name,
					users.email,
					users.admin,
					users.last_login
				order by lastName asc, firstName asc
			), counter as (
				select count(data.id) as total from data
			) 
			select data.*, counter.total 
			from data, counter 
		";
		if (arguments.max > 0) {
			sql &= " limit :max ";
			qry.addParam(name: "max", value=arguments.max, cfsqltype: 'cf_sql_integer');
		} 
		if (arguments.offset > 0) {
			sql &= " offset :offset ";
			qry.addParam(name: "offset", value=arguments.offset, cfsqltype: 'cf_sql_integer');
		} 
		return qry.setSQL(sql).execute().getResult();
		
	}
}