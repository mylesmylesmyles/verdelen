CREATE TABLE public.repeat_entries
(
   id character varying(36) NOT NULL, 
   accounts_id character varying(36) NOT NULL, 
   amount double precision NOT NULL, 
   entry_types_id integer NOT NULL, 
   entry_frequency_id integer NOT NULL, 
   entry_frequencies_count integer NOT NULL DEFAULT 1, 
   entry_frequencies_interval integer NOT NULL DEFAULT 0, 
   entry_frequencies_start_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   entry_frequencies_end_at timestamp without time zone, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT repeat_entries_pk PRIMARY KEY (id), 
   CONSTRAINT repeat_entreies_accounts_fk FOREIGN KEY (accounts_id) REFERENCES accounts (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_types_fk FOREIGN KEY (entry_types_id) REFERENCES entry_types (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_frequency_fk FOREIGN KEY (entry_frequency_id) REFERENCES entry_frequencies (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_created_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT repeat_entries_modified_fk FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.repeat_entries
  OWNER TO verdelen;
COMMENT ON COLUMN public.repeat_entries.id IS 'The primary key';
COMMENT ON COLUMN public.repeat_entries.accounts_id IS 'The account the entry will be placed in';
COMMENT ON COLUMN public.repeat_entries.amount IS 'The entry amount';
COMMENT ON COLUMN public.repeat_entries.entry_types_id IS 'The entry type';
COMMENT ON COLUMN public.repeat_entries.entry_frequency_id IS 'The frequency of the entry';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_count IS 'The number of the entry frequencies';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_interval IS 'The entry frequency interval';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_start_at IS 'The date/time when entries will begin';
COMMENT ON COLUMN public.repeat_entries.entry_frequencies_end_at IS 'The date/time when the last entry will be created';
COMMENT ON COLUMN public.repeat_entries.notes IS 'Notes about the entry';
COMMENT ON COLUMN public.repeat_entries.created_at IS 'The date/time when record was created';
COMMENT ON COLUMN public.repeat_entries.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.repeat_entries.modified_at IS 'The date/time the record was last modified';
COMMENT ON COLUMN public.repeat_entries.modified_users_id IS 'The user who last modified the record';
COMMENT ON TABLE public.repeat_entries
  IS 'Repeating account entries';

CREATE TABLE public.repeat_entries_events
(
   id serial NOT NULL, 
   repeat_entries_id character varying(36) NOT NULL, 
   repeat_year integer, 
   repeat_month integer, 
   repeat_week integer, 
   repeat_day integer, 
   repeat_weekday integer, 
   CONSTRAINT repeat_entries_events_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
COMMENT ON COLUMN public.repeat_entries_events.id IS 'Primary key';
ALTER TABLE repeat_entries_events
  ADD CONSTRAINT repeat_entries_events_fk FOREIGN KEY (repeat_entries_id) REFERENCES repeat_entries (id) ON UPDATE NO ACTION ON DELETE CASCADE;
