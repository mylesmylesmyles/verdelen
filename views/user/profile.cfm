<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.user";
    param name="prc.xhChangePassword" type="string";
    param name="prc.xhChangePassword" type="string";
    param name="prc.successMessage" type="string" default="";
    param name="prc.errorMessages" type="array" default=[];
  </cfscript>
</cfsilent>
<cfoutput>
<cfif prc.successMessage.len()>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div><span class="glyphicon glyphicon-exclamation-ok" aria-hidden="true"></span>#prc.successMessage#</div>
  </div>
</cfif>
<cfif prc.errorMessages.len()>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
    <ul>
      <cfscript>
        #prc.errorMessages.each(function (msg) {
          echo("<li>#msg#</li>")
        })#
      </cfscript>
    </ul>
  </div>
</cfif>
<div class="row">

  <div class="panel panel-info col-md-7">
    <h3 class="panel-heading">About you</h3>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Name:</strong></div>
        <div class="col-md-8 col-sm-12">#prc.user.firstName# #prc.user.lastName#</div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Email:</strong></div>
        <div class="col-md-8 col-sm-12">#prc.user.email#</div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-12"><strong>Last login:</strong></div>
        <div class="col-md-8 col-sm-12">#dateFormat(prc.user.lastLogin, 'yyyy-mm-dd')# #timeFormat(prc.user.lastLogin, 'HH:mm:ss')#</div>
      </div>
    </div>
  </div>
  
  <div class="panel panel-info col-md-4 col-md-offset-1">
    <h3 class="panel-heading">Change your password</h3>

    <div class="panel-body">

    <form action="#prc.xhChangePassword#" method="post" class="form-horizontal">
      <div class="form-group">
        <label for="password" class="control-label sr-only">Enter your current password</label>
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="basic-addon1">&bull;&bull;&bull;</span>
          <input type="password" id="password" name="password" placeholder="Current password" required class="form-control"/>
        </div>
      </div>
      <div class="form-group">
        <label for="new_password" class="control-label sr-only">Enter your new password</label>
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="basic-addon1">&bull;&bull;&bull;</span>
          <input type="password" id="new_password" name="new_password" placeholder="New password" required class="form-control"/>
        </div>
      </div>
      <div class="form-group">
        <label for="new_password_confirm" class="control-label sr-only">Confirm your new current password</label>
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="basic-addon1">&bull;&bull;&bull;</span>
          <input type="password" id="new_password_confirm" name="new_password_confirm" placeholder="Confirm new password" required class="form-control"/>
        </div>
      </div>
      <div class="form-group">
        <div class="text-center">
          <button type="submit" class="btn btn-success btn-lg col-md-12">Change password</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>
</cfoutput>