<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.xhCreateAccount" type="string";
    param name="prc.budget" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<h1 class="text-center">Budget: #esapiEncode('html', prc.budget.name)#</h1>
<div class="row">
  <div class="col-md-3">
    Current balance:
    #LSCurrencyFormat(prc.budget.getBalance())#
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <a href="#event.buildLink("accounts/new")#" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create account</a>
  </div>
  <div class="col-md-3">
    <a href="#event.buildLink('accounts/')#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List accounts</a>
  </div>
  <div class="col-md-3">
    <a href="#event.buildLink('transfers/new')#" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Transfer funds</a>
  </div>
  <div class="col-md-3">
    <a href="#event.buildLink('transfers/')#" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List transfers</a>
  </div>
</div>
</cfoutput>