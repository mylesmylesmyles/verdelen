CREATE TABLE public.entries
(
   id character varying(36) NOT NULL, 
   amount double precision NOT NULL DEFAULT 0, 
   accounts_id character varying(36) NOT NULL, 
   occurred_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   entry_types_id integer NOT NULL, 
   notes text, 
   created_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   created_users_id character varying(36) NOT NULL, 
   modified_at timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP, 
   modified_users_id character varying(36) NOT NULL, 
   CONSTRAINT entries_pk PRIMARY KEY (id), 
   CONSTRAINT entries_modified_users_id_fk FOREIGN KEY (modified_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE RESTRICT, 
   CONSTRAINT entries_created_users_id_fk FOREIGN KEY (created_users_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE RESTRICT, 
   CONSTRAINT entries_types_id_fk FOREIGN KEY (entry_types_id) REFERENCES entry_types (id) ON UPDATE NO ACTION ON DELETE CASCADE
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.entries
  OWNER TO verdelen;
COMMENT ON COLUMN public.entries.id IS 'The primary key';
COMMENT ON COLUMN public.entries.amount IS 'The entry amount';
COMMENT ON COLUMN public.entries.accounts_id IS 'FK to ACCOUNTS.ID';
COMMENT ON COLUMN public.entries.occurred_at IS 'The date/time the entry occurred';
COMMENT ON COLUMN public.entries.entry_types_id IS 'FK to ENTRY_TYPES.ID';
COMMENT ON COLUMN public.entries.notes IS 'Any notes regarding the entry';
COMMENT ON COLUMN public.entries.created_at IS 'The date/time the entry was created';
COMMENT ON COLUMN public.entries.created_users_id IS 'FK to USERS.ID';
COMMENT ON COLUMN public.entries.modified_at IS 'The date/time the entry was last modified';
COMMENT ON COLUMN public.entries.modified_users_id IS 'FK to USERS.ID';
COMMENT ON TABLE public.entries
  IS 'Account entries';
