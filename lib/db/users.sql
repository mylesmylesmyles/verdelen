CREATE TABLE public.users
(
   id character varying(36) NOT NULL, 
   last_name character varying(30) NOT NULL, 
   first_name character varying(30) NOT NULL, 
   email character varying(320) NOT NULL, 
   admin boolean NOT NULL DEFAULT FALSE, 
   is_active boolean NOT NULL DEFAULT TRUE, 
   last_login timestamp without time zone, 
   pass character varying(100), 
   salt character varying(100), 
   CONSTRAINT users_pk PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE public.users
  OWNER TO verdelen;
COMMENT ON COLUMN public.users.first_name IS 'The account first name';
COMMENT ON COLUMN public.users.last_name IS 'The account surname';
COMMENT ON COLUMN public.users.email IS 'The email address';
COMMENT ON COLUMN public.users.admin IS 'Is the user an administrator?';
COMMENT ON COLUMN public.users.is_active IS 'Is the user account active?';
COMMENT ON COLUMN public.users.last_login IS 'The date time the last time the account logged in';
COMMENT ON COLUMN public.users.pass IS 'The hashed account password';
COMMENT ON COLUMN public.users.salt IS 'The password encryption salt';
COMMENT ON TABLE public.users
  IS 'Application user accounts';
