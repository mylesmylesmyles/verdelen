<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.entry" type="any";
  </cfscript>
</cfsilent>
<cfoutput>
<div class="well well-sm">
<h1 class="text-center">Repeat <a href="#event.buildLink('accounts/#prc.entry.account.id#')#">#esapiEncode('html',prc.entry.account.name)#</a> entry </h1>
</div>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3">
    <p class="text-danger">Are you certain you want to delete this entry?  This action can't be undone.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3 text-center">
    <div class="btn-group btn-group-lg btn-group-justified" role="group" aria-label="Actions">
      <form action="#event.buildLink('accounts/#prc.entry.account.id#/repeatentries/#prc.entry.id#/delete')#" method="post" class="form-horizontal">
      <a href="#event.buildLink('accounts/#prc.entry.account.id#/repeatentries/#prc.entry.id#')#" class="btn btn-primary">Cancel</a>
      <input type="submit" value="Delete" class="btn btn-danger"/>
      </form>
    </div>
</div>
</cfoutput>