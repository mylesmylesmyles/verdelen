/**
*
* @file  /handlers/RepeatEntries.cfc
* @author Myles Goodhue
* @description
*
*/

component extends="coldbox.system.EventHandler" autowire=true {

	this.allowedMethods = { 
        view = "GET",
        list = "GET,POST",
        edit = "GET",
        update = "POST,PUT",
        createForm = "GET",
        create = "POST",
        verifyDelete = "GET",
        delete = "POST,DELETE"
    };

    void function preHandler(event) {
        prc.budget = entityNew('Budget').getCurrent(owner: prc.user);
        if (isNull(prc.budget)) {
            setNextEvent('404');
        }
        event.paramValue( name='accountid', value='' );
        prc.account = entityNew('Account').get(rc.accountid);
        if (isNull(prc.account.id)) {
            setNextEvent('404');
        }
    }

    void function view(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('RepeatEntry').get(rc.id);
        if (isNull(prc.entry)) {
            setNextEvent('404');
        }
    }

    void function edit(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('RepeatEntry').get(rc.id);
        if (isNull(prc.entry)) {
            setNextEvent('404');
        }
        prc.xhSubmit = event.buildLink('accounts/#prc.account.id#/repeatentries/#prc.entry.id#');
        prc.entryTypeList = entityNew('EntryType').list();
        prc.entryFrequencyList = entityNew('EntryFrequency').list();
    }
    
    void function list(event,rc,prc) {
        event.paramValue( name='budgetid', value='' );
        prc.budget = entityNew('Budget').get(rc.budgetid);
        if (isNull(prc.budget)) {
            setNextEvent('404');
        }
        event.paramValue( name='accountid', value='' );
        prc.account = entityNew('Account').get(rc.accountid);
        if (isNull(prc.account)) {
            setNextEvent('404');
        }
        prc.entryList = entityNew('RepeatEntry').newCriteria()
            .createAlias("account","a")
                .eq("a.id", prc.account.id)
                .list();
    }
    
    void function update(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('RepeatEntry').get(rc.id);
        if (isNull(prc.entry)) {
            setNextEvent('404');
        }
        event.paramValue( name='amount', value='' );
        event.paramValue( name='type', value='' );
        event.paramValue( name='repeat', value=0 );
        event.paramValue( name='frequency', value='' );
        event.paramValue( name='frequencyCount', value='' );
        event.paramValue( name='frequencyStartAt', value='' );
        event.paramValue( name='frequencyEndAt', value='' );

        var includeFields = ['amount,type,frequency,frequencyCount,frequencyStartAt,frequencyEndAt,notes'];

        // Keep track of error messages
        var errorMessages = [];
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!isNumeric(rc.type)) {
            errorMessages.append("You must enter the entry type.");
        } else {
            var entryTypeObj = entityNew('EntryType').get(rc.type);
            if (isNull(entryTypeObj)) {
                errorMessages.append("The entry type selected is not valid.");
            }
        }
        
        if (!isNumeric(rc.frequency)) {
            errorMessages.append("You must select a repeat frequency.");
        } else {
            var frequencyObj = entityNew('EntryFrequency').get(rc.frequency);
            if (isNull(frequencyObj)) {
                errorMessages.append("The repeat frequency selected is not valid.");
            }
        }

        if (!isDate(rc.frequencyStartAt)) {
            errorMessages.append("You must enter the start date of the repeat frequency.");
        }

        if (isDate(rc.frequencyEndAt)) {
            if (!isDate(rc.frequencyStartAt) and dateCompare(rc.frequencyStartAt, rc.frequencyEndAt, 'd') eq 1) {
                errorMessages.append("The frequency start date must be before the frequency end date.");
            }
            includeFields.append('frequencyEndAt');
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('accounts/#prc.account.id#/repeatentries/new');
            return;
        }

        transaction {
            prc.entry.modifiedby = prc.user;
            prc.entry.modifiedon = now();
            prc.entry.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: includeFields.toList(),
                composeRelationships: true
            );
            prc.entry.save(flush: true);
        }
        setNextEvent('accounts/#prc.account.id#/repeatentries/#prc.entry.id#');
    }
    
    void function createForm(event,rc,prc) {
        prc.xhSubmit = event.buildLink('accounts/#prc.account.id#/repeatentries/new');
        prc.entryTypeList = entityNew('EntryType').list();
        prc.entryFrequencyList = entityNew('EntryFrequency').list();
    }
    
    void function create(event,rc,prc) {
        
        event.paramValue( name='amount', value='' );
        event.paramValue( name='type', value='' );
        event.paramValue( name='repeat', value=0 );
        event.paramValue( name='frequency', value='' );
        event.paramValue( name='frequencyCount', value='' );
        event.paramValue( name='frequencyStartAt', value='' );
        event.paramValue( name='frequencyEndAt', value='' );
        
        var includeFields = ['amount,type,frequency,frequencyCount,frequencyStartAt,frequencyEndAt,notes'];

        // Keep track of error messages
        var errorMessages = [];
        
        // Input validation
        if (!isNumeric(rc.amount) or rc.amount <= 0) {
            errorMessages.append("You must enter an amount greater than zero.");
        }
        
        if (!isNumeric(rc.type)) {
            errorMessages.append("You must enter the entry type.");
        } else {
            var entryTypeObj = entityNew('EntryType').get(rc.type);
            if (isNull(entryTypeObj)) {
                errorMessages.append("The entry type selected is not valid.");
            }
        }
        
        if (!isNumeric(rc.frequency)) {
            errorMessages.append("You must select a repeat frequency.");
        } else {
            var frequencyObj = entityNew('EntryFrequency').get(rc.frequency);
            if (isNull(frequencyObj)) {
                errorMessages.append("The repeat frequency selected is not valid.");
            }
        }

        if (!isDate(rc.frequencyStartAt)) {
            errorMessages.append("You must enter the start date of the repeat frequency.");
        }

        if (isDate(rc.frequencyEndAt)) {
            if (!isDate(rc.frequencyStartAt) and dateCompare(rc.frequencyStartAt, rc.frequencyEndAt, 'd') eq 1) {
                errorMessages.append("The frequency start date must be before the frequency end date.");
            }
            includeFields.append('frequencyEndAt');
        }

        if (errorMessages.len()) {
            flash.persistRC(include: includeFields.toList());
            flash.put( name="errorMessages", value=errorMessages, inflateToPRC=true, autoPurge=true );
            setNextEvent('accounts/#prc.account.id#/repeatentries/new');
            return;
        }

        transaction {
            var entry = entityNew('RepeatEntry').new({account: prc.account, createdby: prc.user, modifiedby: prc.user});
            entry.populate(
                memento: rc,
                trustedSetter: true,
                include: includeFields.toList(),
                nullEmptyInclude: includeFields.toList(),
                composeRelationships: true
            );
            entry.save(flush: true);
        }
        setNextEvent('accounts/#prc.account.id#/repeatentries/#entry.id#');
    }

    void function verifyDelete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('RepeatEntry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }
    }
    
    void function delete(event,rc,prc) {
        event.paramValue( name='id', value='' );
        prc.entry = entityNew('RepeatEntry').get(rc.id);
        if (isNull(prc.entry.id)) {
            setNextEvent('404');
        }
        if (prc.account.id neq prc.entry.account.id) {
            setNextEvent('404');
        }
        transaction {
            entityNew('RepeatEntry').deleteById( rc.id );
        }

        setNextEvent('accounts/#prc.account.id#/repeatentries');
    }
}