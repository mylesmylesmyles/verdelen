<cfsilent>
  <cfscript>
    param name="event" type="any";
    param name="rc" type="struct";
    param name="prc" type="struct";

    param name="prc.xhSubmit" type="string";
    param name="prc.errorMessages" type="array" default=[];
  </cfscript>
</cfsilent>
<cfoutput>
<div class="row">
  <div class="well well-lg col-md-6 col-md-offset-3 col-xs-10 col-xs-offset-1">
    <h2 class="text-center">New account</h2>
    <cfif prc.errorMessages.len()>
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong>Warning:</strong> There was an error with your submission</div>
        <ul>
          <cfscript>
            #prc.errorMessages.each(function (msg) {
              echo("<li>#msg#</li>")
            })#
          </cfscript>
        </ul>
      </div>
    </cfif>
    <form action="#prc.xhSubmit#" method="post" class="form-horizontal">
      <div class="form-group">
          <div class="input-group">
            <cfparam name="rc.name" default=""/>
            <input type="text" id="name" name="name" placeholder="Enter your account name" value="#esapiEncode('html_attr', rc.name)#" required class="form-control input-lg"/>
            <span class="input-group-btn">
              <input class="btn btn-success btn-lg" type="submit" value="Create!">
            </span>
        </div>
      </div>
    </form>
  </div>
</div>
</cfoutput>