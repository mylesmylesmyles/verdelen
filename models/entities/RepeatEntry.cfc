/**
*
* @file  /models/entities/RepeatEntry.cfc
* @author Myles Goodhue
* @description A repeatable entry
*
*/

component 	extends="app.modules.cborm.models.ActiveEntity"
			persistent=true
			entityname="RepeatEntry"
			table="repeat_entries" {

	this.constraints = {};

	property 	name="id"
				column="id"
				type="string"
				sqltype="varchar(36)"
				fieldtype="id"
				persistent=true
				generator="uuid"
				length="36"
				required=true
				notnull=true
				update=false;

	this.constraints.id = { type="string", required=true, length="36" };

	property 	name="account"
				fieldtype="many-to-one"
				cfc="Account"
				fkcolumn="accounts_id"
				column="id"
				cascade="delete"
				lazy=true ;

	this.constraints.account = { required=true };

	property 	name="amount"
				column="amount"
				type="numeric"
				ormtype="float"
				precision=6
				scale=2
				sqltype="decimal(6,2)"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true;

	this.constraints.amount = { type="numeric", required=true };

	property 	name="type"
				fieldtype="many-to-one"
				cfc="EntryType"
				fkcolumn="entry_types_id"
				column="id"
				cascade="delete"
				lazy=true ;

	this.constraints.type = { required=true };

	property 	name="frequency"
				fieldtype="many-to-one"
				cfc="EntryFrequency"
				fkcolumn="entry_frequency_id"
				column="id"
				cascade="delete"
				lazy=true;

	this.constraints.frequency = { required=true };

	property 	name="frequencyCount"
				column="entry_frequencies_count"
				type="numeric"
				sqltype="integer"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true
				dbdefault=0;

	this.constraints.amount = { type="numeric", required=true };

	property 	name="frequencyStartAt"
				column="entry_frequencies_start_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true;

	this.constraints.frequencyStartAt = { type="date", required=true };

	property 	name="frequencyEndAt"
				column="entry_frequencies_end_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=false
				notnull=false;

	this.constraints.frequencyEndAt = { type="date", required=false };

	property 	name="notes"
				column="notes"
				type="string"
				sqltype="varchar(4000)"
				fieldtype="column"
				persistent=true
				required=false
				notnull=false;

	this.constraints.notes = { required=true };

	property 	name="createdAt"
				column="created_at"
				type="timestamp"
				sqltype="timestamp"
				fieldtype="column"
				persistent=true
				required=true
				notnull=true
				dbdefault="sysdate"
				insert=false
				update=false;

	this.constraints.createdAt = { type="date", required=true };

	property 	name="createdBy"
				fieldtype="many-to-one"
				cfc="User"
				fkcolumn="created_users_id"
				column="id"
				cascade="delete"
				lazy=true ;

	this.constraints.createdBy = { required=true };

	property 	name="modifiedAt"
				column="modified_at"
				type="date"
				sqltype="timestamp"
				fieldtype="column"
				// source="db"
				// generated=false
				persistent=true
				required=true
				notnull=true;

	this.constraints.modifiedAt = { type="date", required=true };

	property 	name="modifiedBy"
				fieldtype="many-to-one"
				cfc="User"
				fkcolumn="modified_users_id"
				column="id"
				cascade="delete"
				lazy=true ;

	this.constraints.modifiedBy = { required=true };

	void function preInsert() {
		variables.createdAt = now();
		variables.modifiedAt = now();
	}

	void function preUpdate() {
		variables.modifiedAt = now();
	}

	/**
	 * @hint List the repeat entries which would occur on forDate
	 * @forDate.hint The date the repeat entries would be created
	 */
	any function listForDate(date forDate=now(), boolean asQuery=false) {
		var c = newCriteria();
    	return c
    		.isNotNull('frequency')
    		.sqlRestriction("date_trunc('day', entry_frequencies_start_at) <= cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp)")
    		.disjunction([
    			c.restrictions.isNull('frequencyEndAt'),
    			c.restrictions.sqlRestriction("date_trunc('day', entry_frequencies_end_at) >= cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp)")
    		])
    		.sqlRestriction("
				case
					when entry_frequency_id = 1 then -- day
						case when trunc(DATE_PART('day', cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp) - entry_frequencies_start_at)/entry_frequencies_count) =
							DATE_PART('day', cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp) - entry_frequencies_start_at)/entry_frequencies_count
						then 0 else 1 end

					when entry_frequency_id = 2 then -- weekday
						case when
							extract (isodow from cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp))
							= entry_frequencies_count
						then 0 else 1 end

					when entry_frequency_id = 3 then -- week
						case when trunc(DATE_PART('day', cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp) - entry_frequencies_start_at)/(7*entry_frequencies_count)) =
							DATE_PART('day', cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp) - entry_frequencies_start_at)/(7*entry_frequencies_count)
						then 0 else 1 end

					when entry_frequency_id = 4 then -- week in month
						1

					when entry_frequency_id = 5 then -- month
						case when cast(
								#dateFormat(arguments.forDate, 'mm')#
								- extract(month from entry_frequencies_start_at)
							as bigint)
							% entry_frequencies_count = 0
							and #dateFormat(arguments.forDate, 'dd')# = extract(day from entry_frequencies_start_at)
						then 0 else 1 end

					when entry_frequency_id = 6 then -- year
						case when cast(
								#dateFormat(arguments.forDate, 'yyyy')#
								- extract(year from entry_frequencies_start_at)
							as bigint)
							% entry_frequencies_count = 0
							and #dateFormat(arguments.forDate, 'mm')# = extract(month from entry_frequencies_start_at)
							and #dateFormat(arguments.forDate, 'dd')# = extract(day from entry_frequencies_start_at)
						then 0 else 1 end

					when entry_frequency_id = 7 then -- last day of the month
						case when extract(day from (date_trunc('MONTH', to_date('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#', 'YYYY-MM-DD')) + INTERVAL '1 MONTH - 1 day'))
						= extract(day from cast('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#' as timestamp))
						then 0 else 1 end

					when entry_frequency_id = 8 then -- first day of the month
						case when extract(day from (date_trunc('MONTH', to_date('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#', 'YYYY-MM-DD'))))
						= 1
						then 0 else 1 end

					when entry_frequency_id = 9 then -- x day of the month
						case when extract(day from (date_trunc('MONTH', to_date('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#', 'YYYY-MM-DD'))))
						= entry_frequencies_count
						then 0 else 1 end

					when entry_frequency_id = 10 then -- x week of the year
						case when extract(week from (date_trunc('MONTH', to_date('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#', 'YYYY-MM-DD'))))
						= entry_frequencies_count
						then 0 else 1 end

					when entry_frequency_id = 11 then -- x day of the year
						case when extract(doy from (date_trunc('MONTH', to_date('#dateFormat(arguments.forDate, 'yyyy-mm-dd')#', 'YYYY-MM-DD'))))
						= entry_frequencies_count
						then 0 else 1 end

					else -- not supported
						1
					end = 0
    		")
    		.list(asQuery=arguments.asQuery);
	}


}
